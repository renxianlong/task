package cn.idongjia.task.client.mapper;

import cn.idongjia.tianji.pojos.Bankcard;
import cn.idongjia.tianji.query.BankcardSearch;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

@Component("bankcardMapper")
public interface BankcardMapper {
    @Insert("INSERT INTO kp_seller_bankcard (uid,bid,username,no,branch,status,createtm,code)" +
            " VALUES" +
            " (#{uid},#{bid},#{username},#{no},#{branch},#{status},#{createtm},#{code})")
    @Options(useGeneratedKeys = true, keyProperty = "sbid")
    int insert(Bankcard bankcard);

    @Select("select exists(select uid from kp_seller_bankcard where uid=#{uid} and status=2)")
    boolean hasDefault(long uid);

    @Select("SELECT * FROM kp_seller_bankcard WHERE sbid=#{sbid}")
    Bankcard getById(Long sbid);

    @Select("SELECT * FROM kp_seller_bankcard WHERE" +
            " bid=#{bid} and uid=#{uid} and branch=#{branch}" +
            " and username=#{username} and no = #{no}")
    Bankcard getBankcard(Bankcard bankcard);

    @Update("<script>" +
            "UPDATE `kp_seller_bankcard` SET `sbid` = #{sbid}" +
            " <if test=\"uid != null\"> , `uid` = #{uid}</if>" +
            " <if test=\"bid != null\"> , `bid` = #{bid}</if>" +
            " <if test=\"username != null\"> , `username` = #{username}</if>" +
            " <if test=\"no != null\"> , `no` = #{no}</if>" +
            " <if test=\"branch != null\"> , `branch` = #{branch}</if>" +
            " <if test=\"status != null\"> , `status` = #{status}</if>" +
            " <if test=\"createtm != null\"> , `createtm` = #{createtm}</if>" +
            " WHERE `sbid` = #{sbid}" +
            "</script>")
    int update(Bankcard bankcard);

    @Select("<script>" +
            "SELECT COUNT(*) total FROM kp_seller_bankcard " +
            " <where>" +
            " 1=1" +
            "  <if test=\"search!=null and search.uid != null\"> AND `uid` = #{search.uid}</if>" +
            "  <if test=\"search!=null and search.bid != null\"> AND `bid` = #{search.bid}</if>" +
            "  <if test=\"search!=null and search.status != null\">" +
            "    AND `status` = #{search.status}" +
            "  </if>" +
            "  <if test=\"search!=null and search.username != null\">" +
            "    AND `username` LIKE #{search.username}%" +
            "  </if>" +
            "  <if test=\"search!=null and search.no != null\"> AND `no` = #{search.no}</if>" +
            "  <if test=\"search!=null and search.branch != null\">" +
            "    AND `address` LIKE #{search.branch}" +
            "  </if>" +
            "  <if test=\"search!=null and search.statusRange != null\">" +
            "    AND `status`${search.statusRange}" +
            "  </if>" +
            " </where>" +
            "</script>")
    Integer count(@Param("search") BankcardSearch search);

    @Select("<script>" +
            "SELECT * FROM kp_seller_bankcard " +
            " <where>" +
            " 1=1" +
            "  <if test=\"search!=null and search.uid != null\"> AND `uid` = #{search.uid}</if>" +
            "  <if test=\"search!=null and search.bid != null\"> AND `bid` = #{search.bid}</if>" +
            "  <if test=\"search!=null and search.status != null\">" +
            "    AND `status` = #{search.status}" +
            "  </if>" +
            "  <if test=\"search!=null and search.username != null\">" +
            "    AND `username` LIKE #{search.username}%" +
            "  </if>" +
            "  <if test=\"search!=null and search.no != null\"> AND `no` = #{search.no}</if>" +
            "  <if test=\"search!=null and search.branch != null\">" +
            "    AND `address` LIKE #{search.branch}" +
            "  </if>" +
            "  <if test=\"search!=null and search.statusRange != null\">" +
            "    AND `status`${search.statusRange}" +
            "  </if>" +
            " </where>" +
            " <if test=\"search != null and search.orderBy != null\">" +
            " ORDER BY ${search.orderBy} " +
            " </if>" +
            " <if test=\"search != null and search.limit != null\">" +
            " LIMIT ${search.limit} " +
            " </if>" +
            " <if test=\"search != null and search.offset != null\">" +
            " OFFSET ${search.offset} " +
            " </if>" +
            "</script>")
    List<Bankcard> getAll(@Param("search") BankcardSearch search);

    @Update("UPDATE kp_seller_bankcard SET status=0 WHERE sbid=#{id}")
    int delete(Long id);

    @Update("UPDATE kp_seller_bankcard SET status=1 WHERE uid=#{uid} AND status=2")
    int clearDefault(Long uid);

    @Update("UPDATE kp_seller_bankcard SET status=2 WHERE sbid=#{id}")
    int setDefault(Long id);
}
