package cn.idongjia.task.client.pojo;

public class RefundLog {
    private Long rlid;
    private Long rid;
    private Long createtm;
    private Integer adminflag;
    private Long uid;
    private String data;

    public Long getRlid() {
        return rlid;
    }

    public void setRlid(Long rlid) {
        this.rlid = rlid;
    }

    public Long getRid() {
        return rid;
    }

    public void setRid(Long rid) {
        this.rid = rid;
    }

    public Long getCreatetm() {
        return createtm;
    }

    public void setCreatetm(Long createtm) {
        this.createtm = createtm;
    }

    public Integer getAdminflag() {
        return adminflag;
    }

    public void setAdminflag(Integer adminflag) {
        this.adminflag = adminflag;
    }

    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
