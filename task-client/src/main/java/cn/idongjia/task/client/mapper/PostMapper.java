package cn.idongjia.task.client.mapper;

import cn.idongjia.common.query.BaseSearch;
import cn.idongjia.task.client.pojo.Post;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

@Component("postMapper")
public interface PostMapper {

    @Insert("INSERT INTO kp_post (uid,createtm,pictures,content,type,title,status,recommend) VALUES" +
            " (#{uid}, #{createtm}, #{pictures}, #{content}, #{type}, #{title}, #{status}, #{recommend})")
    @Options(useGeneratedKeys = true, keyProperty = "pid")
    int insert(Post post);

    @Select("SELECT * FROM kp_post WHERE pid=#{id}")
    Post getByCode(Long pid);

    @Update("<script>" +
            " UPDATE `kp_post` SET `pid` = #{pid}" +
            " <if test=\"pictures != null\"> , `pictures` = #{pictures}</if>" +
            " <if test=\"content != null\"> , `content` = #{content}</if>" +
            " <if test=\"type != null\"> , `type` = #{type}</if>" +
            " <if test=\"title != null\"> , `title` = #{title}</if>" +
            " <if test=\"recommend != null\"> , `recommend` = #{recommend}</if>" +
            " <if test=\"status != null\"> , `status` = #{status}</if>" +
            " <if test=\"replycnt != null\"> , `replycnt` = #{replycnt}</if>" +
            " <if test=\"collectcnt != null\"> , `collectcnt` = #{collectcnt}</if>" +
            " <if test=\"weight != null\"> , `weight` = #{weight}</if>" +
            " WHERE `pid` = #{pid}" +
            "</script>")
    int update(Post post);

    @Select("<script>" +
            "SELECT COUNT(*) total FROM kp_post " +
            " <where>" +
            " 1=1" +
            " </where>" +
            "</script>")
    Integer count();

    @Select("<script>" +
            "SELECT * FROM kp_post " +
            " <where>" +
            " 1=1" +
            /*
            "  <if test=\"code != null\"> AND `code` = #{code}</if>" +
            "  <if test=\"name != null\"> AND `name` LIKE %#{name}%</if>" +
            */
            " </where>" +
            " <if test=\"search != null and search.orderBy != null\">" +
            " ORDER BY ${search.orderBy} " +
            " </if>" +
            " <if test=\"search != null and search.limit != null\">" +
            " LIMIT ${search.limit} " +
            " </if>" +
            " <if test=\"search != null and search.offset != null\">" +
            " OFFSET ${search.offset} " +
            " </if>" +
            "</script>")
    List<Post> getAll(@Param("search") BaseSearch search);

    @Update("UPDATE kp_post set status=-1 WHERE pid=#{id}")
    int delete(Long id);

}
