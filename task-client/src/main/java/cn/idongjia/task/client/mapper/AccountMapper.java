package cn.idongjia.task.client.mapper;

import cn.idongjia.tianji.pojos.Account;
import cn.idongjia.tianji.pojos.AccountLog;
import cn.idongjia.tianji.query.AccountSearch;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

@Component("accountMapper")
public interface AccountMapper {
    @Insert("INSERT INTO kp_account (uid,leftamount,status,createtm) VALUES" +
            " (#{uid}, #{leftamount},#{status},#{createtm})")
    int insert(Account account);

    @Select("SELECT * FROM kp_account WHERE uid=#{uid}")
    Account getById(Long uid);

    @Select("SELECT * FROM kp_account WHERE uid=#{uid} FOR UPDATE")
    Account getAndLock(Long uid);

    @Update("<script>" +
            "UPDATE `kp_account` SET `uid` = #{uid}" +
            " <if test=\"leftamount != null\"> , `leftamount` = #{leftamount}</if>" +
            " <if test=\"status != null\"> , `status` = #{status}</if>" +
            " <if test=\"createtm != null\"> , `createtm` = #{createtm}</if>" +
            " WHERE `uid` = #{uid}" +
            "</script>")
    int update(Account account);

    @Select("<script>" +
            "SELECT * FROM kp_account " +
            " <where>" +
            " 1=1" +
            "  <if test=\"search!=null and search.status!=null\">" +
            "    AND `status` = #{search.status}" +
            "  </if>" +
            "  <if test=\"search!=null and search.uid!=null\"> AND `uid` = #{search.uid}</if>" +
            " </where>" +
            " <if test=\"search!=null and search.orderBy!=null\">" +
            " ORDER BY ${search.orderBy} " +
            " </if>" +
            " <if test=\"search!=null and search.limit!=null\">" +
            " LIMIT ${search.limit} " +
            " </if>" +
            " <if test=\"search!=null and search.offset!=null\">" +
            " OFFSET ${search.offset} " +
            " </if>" +
            "</script>")
    List<Account> getAll(@Param("search") AccountSearch search);

    @Select("<script>" +
            "SELECT COUNT(*) total FROM kp_account " +
            " <where>" +
            " 1=1" +
            "  <if test=\"account!=null and account.status!=null\">" +
            "    AND `status` = #{account.status}" +
            "  </if>" +
            "  <if test=\"account!=null and account.uid!=null\"> AND `uid` = #{account.uid}</if>" +
            " </where>" +
            "</script>")
    int count(@Param("account") AccountSearch account);

    @Update("<script>" +
            "UPDATE `kp_account` SET " +
            "leftamount=leftamount+#{inc}" +
            " WHERE `uid` = #{uid}" +
            "</script>")
    int inc(@Param("uid") Long uid, @Param("inc") Double inc);

    @Update("<script>" +
            "UPDATE `kp_account` SET " +
            "leftamount=leftamount-#{desc}" +
            " WHERE `uid` = #{uid}" +
            "</script>")
    int desc(@Param("uid") Long uid, @Param("desc") Double desc);

    @Insert("INSERT INTO kp_account_log (createtm,type,amount,remain,adminflag,uid,data) VALUES" +
            " (#{createtm}, #{type},#{amount},#{remain},#{adminflag},#{uid},#{data})")
    @Options(useGeneratedKeys = true, keyProperty = "alid")
    int writeLog(AccountLog log);

    @Select("<script>" +
            "SELECT * FROM kp_account_log " +
            " <where>" +
            " `uid` = #{uid}" +
            "  <if test=\"search!=null and search.type!=null\"> AND `type` = #{search.type}</if>" +
            "  <if test=\"search!=null and search.adminflag!=null\">" +
            "    AND `adminflag` = #{search.adminflag}" +
            "  </if>" +
            " </where>" +
            " <if test=\"search!=null and search.orderBy!=null\">" +
            " ORDER BY ${search.orderBy} " +
            " </if>" +
            " <if test=\"search!=null and search.limit!=null\">" +
            " LIMIT ${search.limit} " +
            " </if>" +
            " <if test=\"search!=null and search.offset!=null\">" +
            " OFFSET ${search.offset} " +
            " </if>" +
            "</script>")
    List<AccountLog> getLogs(@Param("uid") long uid, @Param("search") AccountSearch search);

    @Update("UPDATE kp_account SET status=-1 WHERE uid=#{id}")
    int delete(Long id);
}
