package cn.idongjia.task.client.mapper;

import cn.idongjia.tianji.pojos.Address;
import cn.idongjia.tianji.query.AddressSearch;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component("addressMapper")
public interface AddressMapper {
    @Insert("REPLACE INTO kp_user_address" +
            " (uid,username,mobile,address,code,status,createtm)" +
            " VALUES" +
            " (#{uid}, #{username}, #{mobile}, #{address},#{code},#{status},#{createtm})")
    @Options(useGeneratedKeys = true, keyProperty = "uaid")
    int insert(Address address);

    @Select("select * from kp_user_address" +
            " where uid=#{uid} and username=#{username} and mobile=#{mobile}" +
            " and address=#{address} and code=#{code}")
    Address getAddress(Address address);

    @Select("select exists(select uid from kp_user_address where uid=#{uid} and status=2)")
    boolean hasDefault(long uid);

    @Select("SELECT * FROM kp_user_address WHERE uaid=#{uaid}")
    Address getById(Long uaid);

    @Select("<script>" +
            "SELECT COUNT(*) total FROM kp_address " +
            " <where>" +
            " 1=1" +
            "  <if test=\"search!=null and search.status!=null\">" +
            "    AND `status` = #{search.status}" +
            "  </if>" +
            "  <if test=\"search!=null and search.uid!=null\"> AND `uid` = #{search.uid}</if>" +
            "  <if test=\"search!=null and search.username!=null\">" +
            "    AND `username` LIKE #{search.username}" +
            "  </if>" +
            "  <if test=\"search!=null and search.mobile != null\">" +
            "    AND `mobile` = #{search.mobile}" +
            "  </if>" +
            "  <if test=\"search!=null and search.address != null\">" +
            "    AND `address` = #{search.address}" +
            "  </if>" +
            "  <if test=\"search!=null and search.statusRange != null\">" +
            "    AND `status`${search.statusRange}" +
            "  </if>" +
            " </where>" +
            "</script>")
    int count(@Param("search") AddressSearch search);

    @Update("<script>" +
            "UPDATE `kp_user_address` SET `uaid` = #{uaid}" +
            " <if test=\"uid != null\"> , `uid` = #{uid}</if>" +
            " <if test=\"username != null\"> , `username` = #{username}</if>" +
            " <if test=\"mobile != null\"> , `mobile` = #{mobile}</if>" +
            " <if test=\"address != null\"> , `address` = #{address}</if>" +
            " <if test=\"code != null\"> , `code` = #{code}</if>" +
            " <if test=\"status != null\"> , `status` = #{status}</if>" +
            " <if test=\"createtm != null\"> , `createtm` = #{createtm}</if>" +
            " WHERE `uaid` = #{uaid}" +
            "</script>")
    int update(Address address);

    @Select("<script>" +
            "SELECT * FROM kp_user_address " +
            " <where>" +
            " 1=1" +
            "  <if test=\"search!=null and search.status!=null\">" +
            "    AND `status` = #{search.status}" +
            "  </if>" +
            "  <if test=\"search!=null and search.uid!=null\"> AND `uid` = #{search.uid}</if>" +
            "  <if test=\"search!=null and search.username!=null\">" +
            "    AND `username` LIKE #{search.username}" +
            "  </if>" +
            "  <if test=\"search!=null and search.mobile != null\">" +
            "    AND `mobile` = #{search.mobile}" +
            "  </if>" +
            "  <if test=\"search!=null and search.address != null\">" +
            "    AND `address` = #{search.address}" +
            "  </if>" +
            "  <if test=\"search!=null and search.statusRange != null\">" +
            "    AND `status`${search.statusRange}" +
            "  </if>" +
            " </where>" +
            " <if test=\"search!=null and search.orderBy!=null\">" +
            " ORDER BY ${search.orderBy} " +
            " </if>" +
            " <if test=\"search!=null and search.limit!=null\">" +
            " LIMIT ${search.limit} " +
            " </if>" +
            " <if test=\"search!=null and search.offset!=null\">" +
            " OFFSET ${search.offset} " +
            " </if>" +
            "</script>")
    List<Address> getAll(@Param("search") AddressSearch search);

    @Update("UPDATE kp_user_address SET status=0 WHERE uaid=#{id}")
    int delete(Long id);

    @Update("UPDATE kp_user_address SET status=1 WHERE uid=#{uid} AND status=2")
    int clearDefault(Long uid);

    @Update("UPDATE kp_user_address SET status=2 WHERE uaid=#{id}")
    int setDefault(Long id);

    @Select("select a.username,a.mobile,r.name,r.prefix,a.address,a.uid" +
            " from kp_user_address a left join kp_region r on r.code=a.code where a.uaid=#{aid}")
    Map getAddressByAid(long aid);

}
