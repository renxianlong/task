package cn.idongjia.task.client.support;

import cn.idongjia.article.pojo.TabRecommend;
import cn.idongjia.task.common.constant.Excuters;
import cn.idongjia.task.common.task.ExcuteType;
import cn.idongjia.task.common.task.FaultType;
import cn.idongjia.task.common.task.Task;
import cn.idongjia.task.common.task.Type;
import cn.idongjia.task.provider.common.ITaskProvider;

import javax.annotation.Resource;

import static cn.idongjia.task.client.constant.TaskTypes.TIMER_RECOMMEND;
import static cn.idongjia.task.client.constant.TaskTypes.TIMER_ARTICLE;

public class RecommendTask {
    @Resource
    private ITaskProvider taskProvider;

    public void addRecommend(TabRecommend recommend) {
        Task task = new Task();

        task.setExcuteType(ExcuteType.JAVA);

        task.setExtParam("tid", String.valueOf(recommend.getTid()));
        task.setExtParam("type", String.valueOf(recommend.getType()));
        task.setExtParam("id", String.valueOf(recommend.getId()));
        task.setExtParam("weight", String.valueOf(recommend.getWeight()));
        task.setExtParam("recommendtm", String.valueOf(recommend.getCreatetm()));

        task.setExuterName(Excuters.DONGJIA_EXCUTER);
        task.setFaultType(FaultType.LOST);
        task.setTaskType(TIMER_ARTICLE);
        task.setTriggerTime(recommend.getCreatetm());
        task.setTaskId(generateTaskId(recommend));
        task.setType(Type.TIMING);

        taskProvider.submitTask(task);
    }

    public void updateRecommendWeight(TabRecommend recommend) {
        Task task = new Task();

        task.setExcuteType(ExcuteType.JAVA);

        task.setExtParam("tid", String.valueOf(recommend.getTid()));
        task.setExtParam("type", String.valueOf(recommend.getType()));
        task.setExtParam("id", String.valueOf(recommend.getId()));
        task.setExtParam("weight", String.valueOf(recommend.getWeight()));
        task.setExtParam("recommendtm", String.valueOf(recommend.getCreatetm()));

        task.setExuterName(Excuters.DONGJIA_EXCUTER);
        task.setFaultType(FaultType.LOST);
        task.setTaskType(TIMER_ARTICLE);
        task.setTriggerTime(recommend.getCreatetm());
        task.setTaskId(generateTaskId(recommend));
        task.setType(Type.TIMING);

        taskProvider.updateTask(task);
    }

    public void cancelRecommend(TabRecommend recommend) {
        taskProvider.cancelTask(generateTaskId(recommend));
    }

    private String generateTaskId(TabRecommend recommend) {
        return TIMER_RECOMMEND + recommend.getTid() + recommend.getType() + recommend.getId();
    }
}
