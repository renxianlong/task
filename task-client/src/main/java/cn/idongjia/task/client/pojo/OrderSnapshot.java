package cn.idongjia.task.client.pojo;

import cn.idongjia.tianji.pojos.Item;
import cn.idongjia.tianji.pojos.Order;

public class OrderSnapshot {
    private Order order;
    private Item item;

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }
}
