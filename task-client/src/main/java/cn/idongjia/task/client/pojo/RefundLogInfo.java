package cn.idongjia.task.client.pojo;

/**
 * Created by renxianlong on 16/1/27.
 */
public class RefundLogInfo {

    private Integer oldStatus;
    private Integer newStatus;
    private Long oldTm;
    private Long uid;
    private Integer adminflag;
    private Long rid;

    public RefundLogInfo(Integer oldStatus, Integer newStatus, Long oldTm, Long uid, Integer adminflag, Long rid) {
        this.oldStatus = oldStatus;
        this.newStatus = newStatus;
        this.oldTm = oldTm;
        this.uid = uid;
        this.adminflag = adminflag;
        this.rid = rid;
    }

    public Integer getOldStatus() {
        return oldStatus;
    }

    public void setOldStatus(Integer oldStatus) {
        this.oldStatus = oldStatus;
    }

    public Integer getNewStatus() {
        return newStatus;
    }

    public void setNewStatus(Integer newStatus) {
        this.newStatus = newStatus;
    }

    public Long getOldTm() {
        return oldTm;
    }

    public void setOldTm(Long oldTm) {
        this.oldTm = oldTm;
    }

    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }

    public Integer getAdminflag() {
        return adminflag;
    }

    public void setAdminflag(Integer adminflag) {
        this.adminflag = adminflag;
    }

    public Long getRid() {
        return rid;
    }

    public void setRid(Long rid) {
        this.rid = rid;
    }
}
