package cn.idongjia.task.client.pojo;

import java.io.Serializable;

/**
 * kp_search_push 表相关信息
 */
public class SearchPush implements Serializable{
    public static enum Status
    {
        SEARCHPUSH_DELETE(-1), SEARCHPUSH_UNDO(0), SEARCHPUSH_DOING(1), SEARCHPUSH_DOWNSUCCESS(2),SEARCHPUSH_DOWNFAILD(3);

        private Integer code;
        private Status(int code) {
            this.code = code;
        }
        public Integer get()
        {
            return code;
        }
    };


    protected Long id;//key

    protected String pojoname;//push de pojo name 比如 ***.Item

    protected Long lid;//上次push最大id

    protected int status;//-1 删除 0 未运行 1 正在运行 2 运行完成成功 3 运行完成失败

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getPojoname() {

        return pojoname;
    }

    public void setPojoname(String pojoname) {
        this.pojoname = pojoname;
    }

    public Long getLid() {
        return lid;
    }

    public void setLid(Long lid) {
        this.lid = lid;
    }

    public Long getId() {

        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
