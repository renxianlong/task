package cn.idongjia.task.client.mapper;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component("utilsMapper")
public interface UtilsMapper {
    @Insert("INSERT INTO kp_unprocessed (uid,targetid,createtm,type,endtm, data) VALUES" +
            " (#{uid}, #{targetid},#{createtm},#{type},#{endtm},#{data})")
    int insert(@Param("uid") long uid, @Param("targetid") String taragetid,
               @Param("createtm") long createtm, @Param("type") int type,
               @Param("endtm") long endtm, @Param("data") String data);

    @Select("select " +
            "s.username sellername,o.uid, o.suid,u.username buyername," +
            "c.form,s.mobile sellermobile,u.mobile buyermobile" +
            " from kp_item_order o" +
            " left join kp_user u on u.uid=o.uid " +
            " left join kp_user s on s.uid=o.suid " +
            " left join kp_craftsman c on c.uid=s.uid" +
            " where o.ooid=#{ooid}")
    Map getUnprocessedData(String ooid);

    @Select("<script>" +
            " SELECT uid, username, avatar FROM {{%user}} WHERE uid in " +
            " <foreach collection=\"uids\" index=\"index\" item=\"item\" " +
            " open=\"(\" separator=\",\" close=\")\">" +
            " #{item} " +
            "</foreach>" +
            "</script>")
    List<Map> getMessageExt(List<String> uids);

    @Select("<script>" +
            " SELECT u.username,c.mobile,c.username craftsman" +
            " FROM kp_item_order o " +
            " LEFT JOIN kp_user u ON u.uid=o.uid " +
            " LEFT JOIN kp_user c ON c.uid=o.suid " +
            " WHERE o.ooid=#{ooid}" +
            "</script>")
    Map getSmsNeeds(String ooid);

    @Select("select * from kp_user where uid=#{uid}")
    Map getUserInfo(long uid);

}
