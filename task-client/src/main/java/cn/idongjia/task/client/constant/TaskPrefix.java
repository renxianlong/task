package cn.idongjia.task.client.constant;

/**
 * Created by renxianlong on 16/4/26.
 */
public class TaskPrefix {
    public static final String AUTO_COLLECT = "auto_collect_";
    public static final String AUTO_LIKE = "auto_like_";
}
