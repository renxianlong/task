package cn.idongjia.task.client.mapper;

import cn.idongjia.tianji.pojos.ShipCompany;
import cn.idongjia.tianji.pojos.Shipment;
import cn.idongjia.tianji.query.ShipSearch;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

@Component("shipMapper")
public interface ShipMapper {
    @Insert("INSERT INTO kp_shipment (ooid,cid,no,address,createtm) VALUES" +
            " (#{ooid}, #{cid}, #{no}, #{address}, #{createtm})")
    int insert(Shipment shipment);

    @Select("SELECT * FROM kp_shipment WHERE ooid=#{ooid}")
    Shipment get(String ooid);

    @Update("<script>" +
            " UPDATE `kp_shipment` SET `ooid` = #{ooid}" +
            " <if test=\"cid != null\"> , `cid` = #{cid}</if>" +
            " <if test=\"no != null\"> , `no` = #{no}</if>" +
            " <if test=\"address != null\"> , `address` = #{address}</if>" +
            " WHERE `ooid` = #{ooid}" +
            "</script>")
    int update(Shipment shipment);

    @Select("<script>" +
            "SELECT COUNT(*) total FROM kp_shipment " +
            " <where>" +
            " 1=1" +
            "  <if test=\"search != null and search.no != null\"> AND `no` = #{search.no}</if>" +
            "  <if test=\"search != null and search.address != null\">" +
            "    AND `address` LIKE  #{address.no}" +
            "  </if>" +
            " </where>" +
            "</script>")
    Integer count(@Param("search") ShipSearch search);

    @Select("<script>" +
            "SELECT * FROM kp_shipment " +
            " <where>" +
            " 1=1" +
            "  <if test=\"search != null and search.no != null\"> AND `no` = #{search.no}</if>" +
            "  <if test=\"search != null and search.address != null\">" +
            "    AND `address` LIKE  #{address.no}" +
            "  </if>" +
            " </where>" +
            " <if test=\"search != null and search.orderBy != null\">" +
            " ORDER BY ${search.orderBy} " +
            " </if>" +
            " <if test=\"search != null and search.limit != null\">" +
            " LIMIT ${search.limit} " +
            " </if>" +
            " <if test=\"search != null and search.offset != null\">" +
            " OFFSET ${search.offset} " +
            " </if>" +
            "</script>")
    List<Shipment> getAll(@Param("search") ShipSearch search);

    @Update("DELETE FROM kp_shipment WHERE ooid=#{id}")
    int delete(String id);

    @Insert("INSERT INTO kp_ship_company (code,name,status,pic,data,createtm) VALUES" +
            " (#{code}, #{name}, #{status},#{pic},#{data},#{createtm})")
    @Options(useGeneratedKeys = true, keyProperty = "cid")
    int insertCompany(ShipCompany shipCompany);

    @Select("<script>" +
            "SELECT * FROM kp_ship_company " +
            " <where>" +
            " 1=1" +
            "  <if test=\"search !=null and search.name != null\">" +
            "    AND `name` LIKE #{search.name}" +
            "  </if>" +
            " </where>" +
            " <if test=\"search != null and search.orderBy != null\">" +
            " ORDER BY ${search.orderBy} " +
            " </if>" +
            " <if test=\"search != null and search.limit != null\">" +
            " LIMIT ${search.limit} " +
            " </if>" +
            " <if test=\"search != null and search.offset != null\">" +
            " OFFSET ${search.offset} " +
            " </if>" +
            "</script>")
    List<ShipCompany> getCompanies(@Param("search") ShipSearch search);

    @Update("DELETE FROM kp_ship_company WHERE cid=#{id}")
    int deleteCompany(Long id);

    @Select("SELECT * FROM kp_ship_company WHERE cid=#{id}")
    ShipCompany getCompany(Long id);
}
