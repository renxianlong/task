package cn.idongjia.task.client.constant;

/**
 * Created by renxianlong on 16/4/26.
 */
public class TaskTypes {
    public static final String AUTH_COLLECT = "auto_collect";
    public static final String AUTH_LIKE = "auto_like";
    public static final String TIMER_RECOMMEND = "timer_recommend";
    public static final String TIMER_ARTICLE = "timer_article";
}
