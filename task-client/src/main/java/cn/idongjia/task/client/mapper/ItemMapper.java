package cn.idongjia.task.client.mapper;

import cn.idongjia.auction.pojos.Auction;
import cn.idongjia.task.client.pojo.Post;
import cn.idongjia.tianji.pojos.*;
import cn.idongjia.tianji.query.BaseSearch;
import cn.idongjia.tianji.query.EvaluateSearch;
import cn.idongjia.tianji.query.ItemSearch;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.Set;

@Component("itemMapper")
public interface ItemMapper {
    @Insert("INSERT INTO kp_item (pid,uid,createtm,updatetm,title,category,pictures,`desc`," +
            "`type`,stock,price,services,evlcnt,`status`,starttm, data) VALUES" +
            " (#{pid},#{uid},#{createtm},#{updatetm},#{title},#{category},#{pictures},#{desc}," +
            "#{type},#{stock},#{price},#{services},#{evlcnt},#{status},#{starttm},#{data})")
    @Options(useGeneratedKeys = true, keyProperty = "iid")
    int insert(Item item);

    @Select("SELECT * FROM kp_item WHERE iid=#{id}")
    Item get(Long id);

    @Select("SELECT * FROM kp_item WHERE iid=#{id} FOR UPDATE")
    Item getAndLock(Long id);

    @Select("<script>" +
            "SELECT * FROM kp_item " +
            " <where>" +
            " type &lt;&gt; 3" +
            " <if test=\"iids != null\"> AND iid IN " +
            "  <foreach collection=\"iids\" index=\"index\" item=\"item\" " +
            "   open=\"(\" separator=\",\" close=\")\">" +
            "       #{item} " +
            "  </foreach>" +
            " </if>" +
            " </where>" +
            " <if test=\"search != null and search.orderBy != null\">" +
            " ORDER BY ${search.orderBy} " +
            " </if>" +
            " <if test=\"search != null and search.limit != null\">" +
            " LIMIT ${search.limit} " +
            " </if>" +
            " <if test=\"search != null and search.offset != null\">" +
            " OFFSET ${search.offset} " +
            " </if>" +
            "</script>")
    List<Item> batchGetByIids(@Param("iids") String[] iids, @Param("search") BaseSearch search);

    @Select("<script>" +
            "SELECT * FROM kp_item " +
            " <where>" +
            " type &lt;&gt; 3" +
            " <if test=\"pids != null\"> AND pid IN " +
            "  <foreach collection=\"pids\" index=\"index\" item=\"item\" " +
            "   open=\"(\" separator=\",\" close=\")\">" +
            "       #{item} " +
            "  </foreach>" +
            " </if>" +
            " </where>" +
            " <if test=\"search != null and search.orderBy != null\">" +
            " ORDER BY ${search.orderBy} " +
            " </if>" +
            " <if test=\"search != null and search.limit != null\">" +
            " LIMIT ${search.limit} " +
            " </if>" +
            " <if test=\"search != null and search.offset != null\">" +
            " OFFSET ${search.offset} " +
            " </if>" +
            "</script>")
    List<Item> batchGetByPids(@Param("pids") String[] pids, @Param("search") BaseSearch search);

    @Update("<script>" +
            " UPDATE `kp_item` SET `iid` = #{iid}" +
            " <if test=\"pid != null\"> , `pid` = #{pid}</if>" +
            " <if test=\"uid != null\"> , `uid` = #{uid}</if>" +
            " <if test=\"updatetm != null\"> , `updatetm` = #{updatetm}</if>" +
            " <if test=\"title != null\"> , `title` = #{title}</if>" +
            " <if test=\"category != null\"> , `category` = #{category}</if>" +
            " <if test=\"pictures != null\"> , `pictures` = #{pictures}</if>" +
            " <if test=\"desc != null\"> , `desc` = #{desc}</if>" +
            " <if test=\"type != null\"> , `type` = #{type}</if>" +
            " <if test=\"stock != null\"> , `stock` = #{stock}</if>" +
            " <if test=\"price != null\"> , `price` = #{price}</if>" +
            " <if test=\"services != null\"> , `services` = #{services}</if>" +
            " <if test=\"evlcnt != null\"> , `evlcnt` = #{evlcnt}</if>" +
            " <if test=\"status != null\"> , `status` = #{status}</if>" +
            " <if test=\"data != null\"> , `data` = #{data}</if>" +
            " <if test=\"starttm != null\"> , `starttm` = #{starttm}</if>" +
            " WHERE `iid` = #{iid}" +
            "</script>")
    int update(Item item);

    @Select("<script>" +
            "SELECT COUNT(*) total FROM kp_item " +
            " <where>" +
            " type &lt;&gt; 3" +
            "  <if test=\"search!=null and search.pid!=null\"> AND `pid` = #{search.pid}</if>" +
            "  <if test=\"search!=null and search.uid!=null\"> AND `uid` = #{search.uid}</if>" +
            "  <if test=\"search!=null and search.category!=null\"> AND `category` = #{search.category}</if>" +
            "  <if test=\"search!=null and search.title!=null\"> AND `title` LIKE #{search.title}</if>" +
            "  <if test=\"search!=null and search.desc != null\"> AND `desc` LIKE #{search.desc}</if>" +
            "  <if test=\"search!=null and search.type!=null\"> AND `type` = #{search.type}</if>" +
            "  <if test=\"search!=null and search.status!=null\"> AND `status` = #{search.status}</if>" +
            "  <if test=\"search!=null and search.startd != null\"> AND `createtm` &gt; #{search.startd}</if>" +
            "  <if test=\"search!=null and search.endd != null\"> AND `createtm` &lt; #{search.endd}</if>" +
            "  <if test=\"search!=null and search.statusRange != null\"> AND `status`${search.statusRange}</if>" +
            " </where>" +
            "</script>")
    Integer count(@Param("search") ItemSearch search);

    @Select("<script>" +
            "SELECT * FROM kp_item " +
            " <where>" +
            " 1=1" +
            "  <if test=\"search!=null and search.pid!=null\"> AND `pid` = #{search.pid}</if>" +
            "  <if test=\"search!=null and search.uid!=null\"> AND `uid` = #{search.uid}</if>" +
            "  <if test=\"search!=null and search.iid!=null\"> AND `iid` = #{search.iid}</if>" +
            "  <if test=\"search!=null and search.category!=null\"> AND `category` = #{search.category}</if>" +
            "  <if test=\"search!=null and search.title!=null\"> AND `title` LIKE #{search.title}</if>" +
            "  <if test=\"search!=null and search.desc != null\"> AND `desc` LIKE #{search.desc}</if>" +
            "  <if test=\"search!=null and search.type!=null\"> AND `type` = #{search.type}</if>" +
            "  <if test=\"search!=null and search.status!=null\"> AND `status` = #{search.status}</if>" +
            "  <if test=\"search!=null and search.startd != null\"> AND `createtm` &gt; #{search.startd}</if>" +
            "  <if test=\"search!=null and search.startd != null\"> AND `createtm` &gt; #{search.startd}</if>" +
            "  <if test=\"search!=null and search.endd != null\"> AND `createtm` &lt; #{search.endd}</if>" +
            "  <if test=\"search!=null and search.statusRange != null\"> AND `status`${search.statusRange}</if>" +
            "  <if test=\"search!=null and search.cidRange != null\"> AND `category` ${search.cidRange}</if>" +
            " </where>" +
            " <if test=\"search != null and search.orderBy != null\">" +
            " ORDER BY ${search.orderBy} " +
            " </if>" +
            " <if test=\"search != null and search.limit != null\">" +
            " LIMIT ${search.limit} " +
            " </if>" +
            " <if test=\"search != null and search.offset != null\">" +
            " OFFSET ${search.offset} " +
            " </if>" +
            "</script>")
    List<Item> getAll(@Param("search") ItemSearch search);

    @Select("<script>" +
            "SELECT *,c.name categoryName,pc.name parentCategory,GROUP_CONCAT(DISTINCT(t.title)) as tag" +
            " FROM kp_item i" +
            " left join kp_post p on p.pid=i.pid" +
            " left join kp_item_category c on c.icid=i.category" +
            " left join kp_item_category pc on pc.icid=c.parentid" +
            " left join kp_tag_rel tl on tl.pid=i.pid " +
            " left join kp_tag t on t.tid=tl.tid" +
            " left join kp_user u on u.uid=i.uid" +
            " <where>" +
            " 1=1" +
            "  <if test=\"search!=null and search.pid!=null\"> AND i.`pid` = #{search.pid}</if>" +
            "  <if test=\"search!=null and search.uid!=null\"> AND i.`uid` = #{search.uid}</if>" +
            "  <if test=\"search!=null and search.iid!=null\"> AND i.`iid` = #{search.iid}</if>" +
            "  <if test=\"search!=null and search.category!=null\"> AND i.`category` = #{search.category}</if>" +
            "  <if test=\"search!=null and search.categoryName!=null\"> AND " +
            "   <choose>" +
            "   <when test=\"search!=null and search.parent!=null and search.parent==0\">" +
            "   pc.`name` LIKE #{search.categoryName}" +
            "   </when>" +
            "   <when test=\"search!=null and search.parent!=null and search.parent==1\">" +
            "   c.`name` LIKE #{search.categoryName}" +
            "   </when>" +
            "   <otherwise>" +
            "   (c.`name` LIKE #{search.categoryName} or pc.`name` LIKE #{search.categoryName})" +
            "   </otherwise>" +
            "   </choose>" +
            "  </if>" +
            "  <if test=\"search!=null and search.tag!=null\"> AND t.`title` LIKE #{search.tag}</if>" +
            "  <if test=\"search!=null and search.username!=null\"> AND u.`username` LIKE #{search.username}</if>" +
            "  <if test=\"search!=null and search.title!=null\"> AND i.`title` LIKE #{search.title}</if>" +
            "  <if test=\"search!=null and search.desc != null\"> AND i.`desc` LIKE #{search.desc}</if>" +
            "  <choose>" +
            "   <when test=\"search!=null and search.type!=null\">" +
            "    AND i.`type` = #{search.type}" +
            "   </when>" +
            "   <otherwise>" +
            "    AND i.`type` &lt;&gt; 3" +
            "   </otherwise>" +
            "  </choose>" +
            "  <if test=\"search!=null and search.status!=null\"> AND i.`status` = #{search.status}</if>" +
            "  <if test=\"search!=null and search.recommend!=null\"> AND p.`recommend` = #{search.recommend}</if>" +
            "  <if test=\"search!=null and search.startd != null\"> AND i.`createtm` &gt;= #{search.startd}</if>" +
            "  <if test=\"search!=null and search.endd != null\"> AND i.`createtm` &lt;= #{search.endd}</if>" +
            "  <if test=\"search!=null and search.rntMin != null\"> AND p.`replycnt` &gt;= #{search.rntMin}</if>" +
            "  <if test=\"search!=null and search.rntMax != null\"> AND p.`replycnt` &lt;= #{search.rntMax}</if>" +
            "  <if test=\"search!=null and search.cntMin != null\"> AND p.`collectcnt` &gt;= #{search.cntMin}</if>" +
            "  <if test=\"search!=null and search.cntMax != null\"> AND p.`collectcnt` &lt;= #{search.cntMax}</if>" +
            "  <if test=\"search!=null and search.evlcntMin != null\"> AND i.`evlcnt` &gt;= #{search.evlcntMin}</if>" +
            "  <if test=\"search!=null and search.evlcntMax != null\"> AND i.`evlcnt` &lt;= #{search.evlcntMax}</if>" +
            "  <if test=\"search!=null and search.priceMin != null\"> AND i.`price` &gt;= #{search.priceMin}</if>" +
            "  <if test=\"search!=null and search.priceMax != null\"> AND i.`price` &lt;= #{search.priceMax}</if>" +
            "  <if test=\"search!=null and search.stockMin != null\"> AND `stock` &gt;= #{search.stockMin}</if>" +
            "  <if test=\"search!=null and search.stockMax != null\"> AND i.`stock` &lt;= #{search.stockMax}</if>" +
            "  <if test=\"search!=null and search.sellMin != null\"> AND `sellcnt` &gt;= #{search.sellMin}</if>" +
            "  <if test=\"search!=null and search.sellMax != null\"> AND i.`sellcnt` &lt;= #{search.sellMax}</if>" +
            "  <if test=\"search!=null and search.statusRange != null\"> AND i.`status`${search.statusRange}</if>" +
            "  <if test=\"search!=null and search.cidRange != null\"> AND i.`category` ${search.cidRange}</if>" +
            "  <choose>" +
            "   <when test=\"search!=null and search.recMin != null and search.recMax != null\">" +
            "   OR (p.`recommendtm` &gt;= #{search.recMin} AND p.`recommendtm` &lt;= #{search.recMax})" +
            "   </when>" +
            "   <when test=\"search!=null and search.recMin != null\">" +
            "   OR p.`recommendtm` &gt;= #{search.recMin}" +
            "   </when>" +
            "   <when test=\"search!=null and search.recMax != null\">" +
            "   OR p.`recommendtm` &lt;= #{search.recMax}" +
            "   </when>" +
            "   <otherwise></otherwise>" +
            "  </choose>" +
            " </where>" +
            " group by p.pid" +
            " <if test=\"search != null and search.orderBy != null\">" +
            " ORDER BY ${search.orderBy} " +
            " </if>" +
            " <if test=\"search != null and search.limit != null\">" +
            " LIMIT ${search.limit} " +
            " </if>" +
            " <if test=\"search != null and search.offset != null\">" +
            " OFFSET ${search.offset} " +
            " </if>" +
            "</script>")
    List<ItemPost> search(@Param("search") ItemSearch search);

    @Select("<script>" +
            "SELECT count(distinct(i.iid))" +
            " FROM kp_item i" +
            " left join kp_post p on p.pid=i.pid" +
            " left join kp_item_category c on c.icid=i.category" +
            " left join kp_item_category pc on pc.icid=c.parentid" +
            " left join kp_tag_rel tl on tl.pid=i.pid " +
            " left join kp_tag t on t.tid=tl.tid" +
            " left join kp_user u on u.uid=i.uid" +
            " <where>" +
            " 1=1" +
            "  <if test=\"search != null and search.pid!=null\"> AND i.`pid` = #{search.pid}</if>" +
            "  <if test=\"search != null and search.uid!=null\"> AND i.`uid` = #{search.uid}</if>" +
            "  <if test=\"search != null and search.iid!=null\"> AND i.`iid` = #{search.iid}</if>" +
            "  <if test=\"search != null and search.category!=null\"> AND i.`category` = #{search.category}</if>" +
            "  <if test=\"search!=null and search.categoryName!=null\"> AND " +
            "   <choose>" +
            "   <when test=\"search!=null and search.parent!=null and search.parent==0\">" +
            "   pc.`name` LIKE #{search.categoryName}" +
            "   </when>" +
            "   <when test=\"search!=null and search.parent!=null and search.parent==1\">" +
            "   c.`name` LIKE #{search.categoryName}" +
            "   </when>" +
            "   <otherwise>" +
            "   (c.`name` LIKE #{search.categoryName} or pc.`name` LIKE #{search.categoryName})" +
            "   </otherwise>" +
            "   </choose>" +
            "  </if>" +
            "  <if test=\"search!=null and search.tag!=null\"> AND t.`title` LIKE #{search.tag}</if>" +
            "  <if test=\"search!=null and search.username!=null\"> AND u.`username` LIKE #{search.username}</if>" +
            "  <if test=\"search != null and search.title!=null\"> AND i.`title` LIKE #{search.title}</if>" +
            "  <if test=\"search != null and search.desc != null\"> AND i.`desc` LIKE #{search.desc}</if>" +
            "  <choose>" +
            "   <when test=\"search!=null and search.type!=null\">" +
            "    AND i.`type` = #{search.type}" +
            "   </when>" +
            "   <otherwise>" +
            "    AND i.`type` &lt;&gt; 3" +
            "   </otherwise>" +
            "  </choose>" +
            "  <if test=\"search != null and search.status!=null\"> AND i.`status` = #{search.status}</if>" +
            "  <if test=\"search!=null and search.recommend!=null\"> AND p.`recommend` = #{search.recommend}</if>" +
            "  <if test=\"search!=null and search.startd != null\"> AND i.`createtm` &gt;= #{search.startd}</if>" +
            "  <if test=\"search!=null and search.endd != null\"> AND i.`createtm` &lt;= #{search.endd}</if>" +
            "  <if test=\"search!=null and search.rntMin != null\"> AND p.`replycnt` &gt;= #{search.rntMin}</if>" +
            "  <if test=\"search!=null and search.rntMax != null\"> AND p.`replycnt` &lt;= #{search.rntMax}</if>" +
            "  <if test=\"search!=null and search.cntMin != null\"> AND p.`collectcnt` &gt;= #{search.cntMin}</if>" +
            "  <if test=\"search!=null and search.cntMax != null\"> AND p.`collectcnt` &lt;= #{search.cntMax}</if>" +
            "  <if test=\"search!=null and search.evlcntMin != null\"> AND i.`evlcnt` &gt;= #{search.evlcntMin}</if>" +
            "  <if test=\"search!=null and search.evlcntMax != null\"> AND i.`evlcnt` &lt;= #{search.evlcntMax}</if>" +
            "  <if test=\"search!=null and search.priceMin != null\"> AND i.`price` &gt;= #{search.priceMin}</if>" +
            "  <if test=\"search!=null and search.priceMax != null\"> AND i.`price` &lt;= #{search.priceMax}</if>" +
            "  <if test=\"search!=null and search.stockMin != null\"> AND `stock` &gt;= #{search.stockMin}</if>" +
            "  <if test=\"search!=null and search.stockMax != null\"> AND i.`stock` &lt;= #{search.stockMax}</if>" +
            "  <if test=\"search!=null and search.sellMin != null\"> AND `sellcnt` &gt;= #{search.sellMin}</if>" +
            "  <if test=\"search!=null and search.sellMax != null\"> AND i.`sellcnt` &lt;= #{search.sellMax}</if>" +
            "  <if test=\"search!=null and search.statusRange != null\"> AND i.`status`${search.statusRange}</if>" +
            "  <if test=\"search!=null and search.cidRange != null\"> AND i.`category` ${search.cidRange}</if>" +
            "  <choose>" +
            "   <when test=\"search!=null and search.recMin != null and search.recMax != null\">" +
            "   OR (p.`recommendtm` &gt;= #{search.recMin} AND p.`recommendtm` &lt;= #{search.recMax})" +
            "   </when>" +
            "   <when test=\"search!=null and search.recMin != null\">" +
            "   OR p.`recommendtm` &gt;= #{search.recMin}" +
            "   </when>" +
            "   <when test=\"search!=null and search.recMax != null\">" +
            "   OR p.`recommendtm` &lt;= #{search.recMax}" +
            "   </when>" +
            "   <otherwise></otherwise>" +
            "  </choose>" +
            " </where>" +
            "</script>")
    int searchCount(@Param("search") ItemSearch search);

    @Update("UPDATE kp_item SET status=-1 WHERE iid=#{iid}")
    int delete(Long id);

    @Update("UPDATE kp_item set stock=stock-#{stock},sellcnt=sellcnt+#{stock} WHERE iid=#{iid}")
    int decrease(@Param("iid") Long iid, @Param("stock") Long stock);

    @Update("UPDATE kp_item set stock=stock+#{stock},sellcnt=sellcnt-#{stock} WHERE iid=#{iid}")
    int increase(@Param("iid") Long iid, @Param("stock") Long stock);

    @Insert("INSERT INTO kp_item_log (iid,createtm,type,adminflag,uid,data) VALUES" +
            " (#{iid},#{createtm},#{type},#{adminflag},#{uid},#{data})")
    @Options(useGeneratedKeys = true, keyProperty = "ilid")
    int writeLog(ItemLog log);

    @Select("<script>" +
            "SELECT * FROM kp_item_log " +
            " <where>" +
            " 1=1" +
            "  <if test=\"iid != null\"> AND `iid` = #{iid}</if>" +
            " </where>" +
            " <if test=\"search != null and search.orderBy != null\">" +
            " ORDER BY ${search.orderBy} " +
            " </if>" +
            " <if test=\"search != null and search.limit != null\">" +
            " LIMIT ${search.limit} " +
            " </if>" +
            " <if test=\"search != null and search.offset != null\">" +
            " OFFSET ${search.offset} " +
            " </if>" +
            "</script>")
    List<ItemLog> getLogs(@Param("iid") Long iid,
                          @Param("search") BaseSearch search);

    @Select("SELECT * FROM kp_item_log WHERE ilid=#{id}")
    ItemLog getLog(Long id);

    @Select("<script>" +
            "select * from kp_item_order_evaluate where iid=#{id} " +
            " order by createtm desc" +
            " <if test=\"search != null and search.limit != null\">" +
            " LIMIT ${search.limit} " +
            " </if>" +
            " <if test=\"search != null and search.offset != null\">" +
            " OFFSET ${search.offset} " +
            " </if>" +
            "</script>")
    List<Evaluate> getItemEvaluates(@Param("id") Long id, @Param("search") BaseSearch search);

    @Select("<script>" +
            "select e.*,i.pid,u.username,c.uid cid,c.username craftsman,i.title,c.name category" +
            " from kp_item_order_evaluate e" +
            " LEFT JOIN kp_item i on i.iid=e.iid" +
            " LEFT JOIN kp_item_order o on o.ooid=e.ooid" +
            " LEFT JOIN kp_user u on u.uid=o.uid" +
            " LEFT JOIN kp_user s on s.uid=o.suid" +
            " LEFT JOIN kp_item_category c on c.cid=i.cid" +
            " <where>" +
            " 1=1 " +
            "  <if test=\"search!=null and search.iid!=null\"> AND i.`iid` = #{search.iid}</if>" +
            "  <if test=\"search!=null and search.pid!=null\"> AND i.`pid` = #{search.pid}</if>" +
            "  <if test=\"search!=null and search.title!=null\"> AND i.`title` like #{search.title}</if>" +
            "  <if test=\"search!=null and search.oid!=null\"> AND o.`ooid` = #{search.oid}</if>" +
            "  <if test=\"search!=null and search.uid!=null\"> AND o.`uid` = #{search.uid}</if>" +
            "  <if test=\"search!=null and search.cid!=null\"> AND o.`suid` = #{search.cid}</if>" +
            "  <if test=\"search!=null and search.username!=null\"> AND u.`username` like #{search.username}</if>" +
            "  <if test=\"search!=null and search.craftsman!=null\"> AND s.`username` like #{search.craftsman}</if>" +
            "  <if test=\"search!=null and search.endd!=null\"> AND e.`createtm` &lt;= #{search.endd}</if>" +
            "  <if test=\"search!=null and search.startd!=null\"> AND e.`createtm` &gt;= #{search.startd}</if>" +
            " </where>" +
            " <if test=\"search != null and search.orderBy != null\">" +
            "  ORDER BY ${search.orderBy} " +
            " </if>" +
            " <if test=\"search != null and search.limit != null\">" +
            "  LIMIT ${search.limit} " +
            " </if>" +
            " <if test=\"search != null and search.offset != null\">" +
            "  OFFSET ${search.offset} " +
            " </if>" +
            "</script>")
    List<Evaluate> getEvaluates(@Param("search") EvaluateSearch search);

    @Select("<script>" +
            "select COUNT(DISTINCT(e.eid)) from kp_item_order_evaluate e" +
            " LEFT JOIN kp_item i on i.iid=e.iid" +
            " LEFT JOIN kp_item_order o on o.ooid=e.ooid" +
            " LEFT JOIN kp_user u on u.uid=o.uid" +
            " LEFT JOIN kp_user c on c.uid=o.suid" +
            " <where>" +
            " 1=1 " +
            "  <if test=\"search!=null and search.iid!=null\"> AND i.`iid` = #{search.iid}</if>" +
            "  <if test=\"search!=null and search.pid!=null\"> AND i.`pid` = #{search.pid}</if>" +
            "  <if test=\"search!=null and search.title!=null\"> AND i.`title` like #{search.title}</if>" +
            "  <if test=\"search!=null and search.oid!=null\"> AND o.`ooid` = #{search.oid}</if>" +
            "  <if test=\"search!=null and search.uid!=null\"> AND o.`uid` = #{search.uid}</if>" +
            "  <if test=\"search!=null and search.cid!=null\"> AND o.`suid` = #{search.cid}</if>" +
            "  <if test=\"search!=null and search.username!=null\"> AND u.`username` like #{search.username}</if>" +
            "  <if test=\"search!=null and search.craftsman!=null\"> AND c.`username` like #{search.craftsman}</if>" +
            "  <if test=\"search!=null and search.endd!=null\"> AND e.`createtm` &lt;= #{search.endd}</if>" +
            "  <if test=\"search!=null and search.startd!=null\"> AND e.`createtm` &gt;= #{search.startd}</if>" +
            " </where>" +
            "</script>")
    int getEvaluateCount(@Param("search") EvaluateSearch search);

    @Select("select count(*) from kp_item_order_evaluate where iid=#{id}")
    int itemEvaluateCount(Long id);

    @Update("update kp_item set evlcnt=evlcnt+1 where iid=#{id}")
    int incrEvaluateCount(long id);

    @Select("<script>" +
            " SELECT p.recommend,p.recommendtm,i.* FROM kp_assemble_rel al" +
            "   LEFT JOIN kp_item_category c ON c.parentid = al.targetid AND al.type = 2" +
            "   LEFT JOIN kp_item i ON (i.category = al.targetid AND al.type = 2) or i.category=c.icid" +
            "   LEFT JOIN kp_user u on u.uid=i.uid" +
            "   LEFT JOIN kp_craftsman cm on cm.uid=i.uid" +
            "   LEFT JOIN kp_post p ON p.pid=i.pid" +
            "  WHERE al.aid = #{tid} and i.status=1 and u.status=4 and cm.status=1 and i.type &lt;&gt; 3" +
            " UNION" +
            "  SELECT p.recommend,p.recommendtm,i.* FROM kp_assemble_rel al" +
            "   LEFT JOIN kp_tag_rel tl ON tl.tid = al.targetid AND al.type = 1" +
            "   LEFT JOIN kp_item i ON i.pid = tl.pid" +
            "   LEFT JOIN kp_user u on u.uid=i.uid" +
            "   LEFT JOIN kp_craftsman c on c.uid=i.uid" +
            "   LEFT JOIN kp_post p ON p.pid=i.pid" +
            "  WHERE al.aid = #{tid} and i.status=1 and u.status=4 and c.status=1 and i.type &lt;&gt; 3" +
            " <if test=\"search != null and search.orderBy != null\">" +
            " ORDER BY ${search.orderBy} " +
            " </if>" +
            " <if test=\"search != null and search.limit != null\">" +
            " LIMIT ${search.limit} " +
            " </if>" +
            " <if test=\"search != null and search.offset != null\">" +
            " OFFSET ${search.offset} " +
            " </if>" +
            "</script>")
    List<Item> getTagItems(@Param("tid") long tid, @Param("search") BaseSearch search);

    @Select("<script>" +
            "select * " +
            " <if test=\"search!=null and search.uid!=null and search.recommend == null\">" +
            "   ,(case when i.stock > 0 then 1 else 0 end) as num" +
            " </if>" +
            " from kp_item i" +
            " left join kp_post p on p.pid=i.pid and p.type=3" +
            " left join kp_user u on u.uid=i.uid " +
            " left join kp_craftsman c on c.uid=i.uid " +
            "   <if test=\"search!=null and search.uid!=null and search.recommend != null\">" +
            "       left join kp_craftsman_items ci on ci.uid=i.uid and ci.iid=i.iid" +
            "   </if>" +
            " <where> " +
            "   u.status=4 and p.status=0 and c.status=1 and i.type &lt;&gt; 3 and c.block=0" +
            "   <if test=\"search!=null and search.status!=null\"> AND i.`status` = #{search.status}</if>" +
            "   <choose>" +
            "   <when test=\"search !=null and search.uid!=null and search.recommend != null\">" +
            "       and ci.uid=#{search.uid} " +
            "   </when>" +
            "   <when test=\"search !=null and search.uid!=null \">" +
            "       and u.uid=#{search.uid} " +
            "   </when>" +
            "   <otherwise>" +
            "   </otherwise>" +
            "   </choose>" +
            " </where>" +
            " group by i.iid " +
            "   <choose>" +
            "   <when test=\"search != null and search.uid != null and search.recommend == null\">" +
            "       ORDER BY num desc, i.updatetm DESC " +
            "   </when>" +
            "   <when test=\"search.uid != null and search.recommend != null\">" +
            "       ORDER BY p.weight DESC, i.createtm DESC " +
            "   </when>" +
            "   <when test=\"search.uid == null and search.recommend != null and search.recommend == 1\">" +
            "       ORDER BY p.recommend desc ,p.recommend desc, i.createtm DESC " +
            "   </when>" +
            "   <otherwise>" +
            "       ORDER BY i.createtm DESC " +
            "   </otherwise>" +
            "   </choose>" +
            " <if test=\"search != null and search.limit != null\">" +
            "   LIMIT ${search.limit} " +
            " </if>" +
            " <if test=\"search != null and search.offset != null\">" +
            "   OFFSET ${search.offset} " +
            " </if>" +
            "</script>")
    List<Item> craftsmans(@Param("search") ItemSearch search);

    @Select("<script>" +
            "SELECT p.pid,p.type tp,u.adminflag af" +
            " from kp_post p " +
            " LEFT JOIN kp_user u ON u.uid=p.uid" +
            " LEFT JOIN kp_craftsman c ON c.uid=u.uid" +
            " LEFT JOIN kp_item i on i.pid=p.pid and (c.status=1 and i.type &lt;&gt; 3 or i.type=3) and i.status=1" +
            " <where>" +
            " p.status=0 and u.status=4 and (p.type=1 and p.title = '' or p.type=3 and i.iid is not null)" +
            " <if test=\"recommends!=null\"> " +
            "  and p.pid in " +
            " <foreach collection=\"recommends\" index=\"index\" item=\"item\" " +
            " open=\"(\" separator=\",\" close=\")\">" +
            "  #{item} " +
            " </foreach>" +
            " </if>" +
            " </where>" +
            " group by p.pid ORDER BY p.weight DESC, p.recommendtm DESC " +
            "</script>")
    List<Feed> getFromPost(@Param("recommends") String[] recommends);

    @Select("<script>" +
            "SELECT p.pid,p.type tp,u.adminflag af" +
            " from kp_post p " +
            " LEFT JOIN kp_user u ON u.uid=p.uid" +
            " LEFT JOIN kp_craftsman c ON c.uid=u.uid" +
            " LEFT JOIN kp_item i on i.pid=p.pid and c.status=1 and i.status=1 and i.type &lt;&gt; 3" +
            " <where>" +
            " p.recommend=1 and p.status=0 and u.status=4 and (p.type=1 and p.title = '' or p.type=3 and i.iid is not null)" +
            " <if test=\"exclude!=null and !exclude.isEmpty()\"> " +
            "  and p.pid not in " +
            " <foreach collection=\"exclude\" index=\"index\" item=\"item\" " +
            " open=\"(\" separator=\",\" close=\")\">" +
            "  #{item} " +
            " </foreach>" +
            " </if>" +
            " </where>" +
            " group by p.pid ORDER BY p.weight DESC, p.recommendtm DESC " +
            " LIMIT #{recommendLimit}, #{recommend} " +
            "</script>")
    List<Feed> getRecommend(@Param("exclude") Set<Long> exclude, @Param("recommend") int recommend,
                            @Param("recommendLimit") int recommendLimit);

    @Select("<script>" +
            "SELECT p.pid,p.type tp,u.adminflag af" +
            " from kp_post p " +
            " LEFT JOIN kp_user u ON u.uid=p.uid" +
            " LEFT JOIN kp_craftsman c ON c.uid=u.uid" +
            " LEFT JOIN kp_item i on i.pid=p.pid and c.status=1 and i.status=1 and i.type &lt;&gt; 3" +
            " <where>" +
            " p.status=0 and u.status=4 and (p.type=1 and p.title = '' or p.type=3 and i.iid is not null) and c.block=0" +
            " and (p.uid IN (SELECT `uid` FROM kp_fans WHERE fansid =#{uid}) or p.uid=#{uid})" +
            " <if test=\"lower\"> and p.createtm > #{limitTime} </if>" +
            " <if test=\"exclude!=null and !exclude.isEmpty()\"> " +
            "  and p.pid not in " +
            " <foreach collection=\"exclude\" index=\"index\" item=\"item\" " +
            " open=\"(\" separator=\",\" close=\")\">" +
            "  #{item} " +
            " </foreach>" +
            " </if>" +
            " </where>" +
            " group by p.pid ORDER BY p.recommend desc, p.createtm DESC" +
            " LIMIT #{followLimit}, #{follows}" +
            "</script>")
    List<Feed> getFeeds(@Param("exclude") Set<Long> exclude, @Param("uid") long uid,
                        @Param("follows") int follows, @Param("followLimit") int followLimit,
                        @Param("lower") boolean lower, @Param("limitTime") long limitTime);

    @Select("SELECT p.pid, p.`type` tp,u.adminflag af" +
            " FROM kp_topic_rel r" +
            " LEFT JOIN kp_post p ON r.pid=p.pid" +
            " LEFT JOIN kp_user u on u.uid=p.uid" +
            " LEFT JOIN kp_item i on i.uid=p.uid" +
            " LEFT JOIN kp_craftsman c ON c.uid=u.uid" +
            " WHERE" +
            " r.tpid=#{tpid} and p.status=0 and u.status=4" +
            " and (p.type=1 or p.type=3 and c.status=1 and i.status=1)" +
            " GROUP BY p.pid" +
            " ORDER BY r.weight DESC,r.createtm DESC ")
    List<Feed> getTopicDetail(int tpid);

    @Select("SELECT i.*,i.pictures cover,u.username,u.avatar,i.starttm auctiontm" +
            " FROM kp_item i" +
            " LEFT JOIN kp_user u ON u.uid=i.uid" +
            " WHERE iid=#{aid} and i.type=3")
    Auction getAuction(Long aid);

    @Select("SELECT u.uid,u.username,u.avatar,c.title FROM kp_user u left join kp_craftsman c on c.uid=u.uid where u.uid=#{uid}")
    Map getUserInfo(long uid);

    @Select("SELECT * FROM kp_post WHERE pid=#{pid}")
    Post getByPid(String pid);

    @Select("SELECT * FROM kp_item WHERE pid=#{pid}")
    Item getItemByPid(long pid);

    @Select("select exists (select pid from kp_item where iid=#{iid} and type=3)")
    boolean isAuction(long iid);
}
