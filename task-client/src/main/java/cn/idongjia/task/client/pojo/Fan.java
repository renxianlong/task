package cn.idongjia.task.client.pojo;

import java.io.Serializable;

/**
 * Created by renxianlong on 16/2/25.
 */
public class Fan implements Serializable{
    private Long uid;
    private Long fansid;
    private Long createtm;

    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }

    public Long getFansid() {
        return fansid;
    }

    public void setFansid(Long fansid) {
        this.fansid = fansid;
    }

    public Long getCreatetm() {
        return createtm;
    }

    public void setCreatetm(Long createtm) {
        this.createtm = createtm;
    }
}
