package cn.idongjia.task.client.pojo;

import java.util.Map;

public class NotifyCache {
    private String tp;
    private String id;
    private int type;
    private int status;
    private String custom;

    private Map<String, String> data;

    public static enum Tp {
        SYSTEM("s");

        private String code;
        private Tp(String code) {
            this.code = code;
        }
        public String get() {
            return this.code;
        }
    }

    public static enum Type {
        ORDER(1), NOTIFY(2), ACTIVITY(3);

        private int code;
        private Type(int code) {
            this.code = code;
        }
        public int get() {
            return this.code;
        }
    }

    public static enum Status {
        SEND(1), RECEIVE(2), CHANGE_PRICE(3), NEW(4),  STOCK_LACED(5), UNDERCARRIAGE(6),
        REFUNDED(7), REFUND_REFUSED(8), REFUND_MASTER(9), PAYED(10),
        CONFIRMED(11), REFUNDING(12), PAY_FAILED(13), NEW_ORDER(14), RETURNING(15),ORDER_CLOSED(16);

        private int code;
        private Status(int code) {
            this.code = code;
        }
        public int get() {
            return this.code;
        }
    }

    public NotifyCache() {
    }

    public NotifyCache(String id, int type, int status) {
        this.id = id;
        this.type = type;
        this.status = status;
    }

    public NotifyCache(String id, int type, int status, Map<String, String> data) {
        this.tp = Tp.SYSTEM.get();
        this.id = id;
        this.type = type;
        this.status = status;
        this.data = data;
    }

    public NotifyCache(String tp, String id, int type, int status, String custom) {
        this.tp = tp;
        this.id = id;
        this.type = type;
        this.status = status;
        this.custom = custom;
    }

    public NotifyCache(String tp, String id, int type, int status, Map<String, String> data) {
        this.tp = tp;
        this.id = id;
        this.type = type;
        this.status = status;
        this.data = data;
    }

    public String getTp() {
        return tp;
    }

    public void setTp(String tp) {
        this.tp = tp;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getCustom() {
        return custom;
    }

    public void setCustom(String custom) {
        this.custom = custom;
    }

    public Map<String, String> getData() {
        return data;
    }

    public void setData(Map<String, String> data) {
        this.data = data;
    }
}
