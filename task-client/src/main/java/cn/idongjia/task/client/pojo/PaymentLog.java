package cn.idongjia.task.client.pojo;

import cn.idongjia.tianji.pojos.Payment;

/**
 * Created by leedongwei on 16/3/16.
 */
public class PaymentLog extends Payment {

    private Long logcreatetime;
    private int logtype;

    public PaymentLog(Payment payment) {
        this.setOoid(payment.getOoid());
        this.setType(payment.getType());
        this.setData(payment.getData());
        this.setTrade(payment.getTrade());
        this.setResult(payment.getResult());
        this.setCardno(payment.getCardno());
        this.setCreatetm(payment.getCreatetm());
        this.setOpid(payment.getOpid());
        this.setPaytm(payment.getPaytm());
        this.setPrepay(payment.getPrepay());
        this.setRealpay(payment.getRealpay());
        this.setUpdatetm(payment.getUpdatetm());
    }

    public PaymentLog() {
    }

    public static enum LogType
    {
        PAY_INSERT(1), PAY_UPDATE(2);

        private Integer code;
        private LogType(int code) {
            this.code = code;
        }
        public Integer get()
        {
            return this.code;
        }
    }

    public Long getLogcreatetime() {
        return logcreatetime;
    }

    public void setLogcreatetime(Long logcreatetime) {
        this.logcreatetime = logcreatetime;
    }

    public int getLogtype() {
        return logtype;
    }

    public void setLogtype(int logtype) {
        this.logtype = logtype;
    }
}
