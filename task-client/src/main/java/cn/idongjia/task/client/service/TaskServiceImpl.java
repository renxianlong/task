package cn.idongjia.task.client.service;

import cn.idongjia.log.Log;
import cn.idongjia.log.LogFactory;
import cn.idongjia.task.common.services.TaskService;
import cn.idongjia.task.common.task.Task;
import cn.idongjia.task.provider.common.ITaskProvider;
import cn.idongjia.task.provider.init.TaskProvider;
import com.alibaba.dubbo.rpc.protocol.rest.support.ContentType;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * Created by renxianlong on 16/4/20.
 */
@Path("/provider")
@Produces({ContentType.APPLICATION_JSON_UTF_8})
@Component("taskServiceImpl")
public class TaskServiceImpl implements TaskService {

    private Log log = LogFactory.getLog(TaskServiceImpl.class);

    @Resource
    private ITaskProvider taskProvider;

    @Override
    @POST
    @Path("/submit")
    @Consumes({MediaType.APPLICATION_JSON})
    public void submitTask(Task task) {
        log.debug("receive submitTask request:" + task);
        taskProvider.submitTask(task);
    }

    @Override
    public void cancelTask(String s) {
        log.debug("receive cancelTask request: taskId = " + s);
        taskProvider.cancelTask(s);
    }

    @Override
    public void updateTask(Task task) {
        log.debug("receive updateTask request:" + task);
        taskProvider.updateTask(task);
    }
}
