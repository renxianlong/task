package cn.idongjia.task.client.pojo;

import java.io.Serializable;

/**
 * Created by renxianlong on 16/2/25.
 */
public class Collect implements Serializable{
    private Long uid;
    private Long targetid;
    private Integer type;
    private Long createtm;

    public Long getTargetid() {
        return targetid;
    }

    public void setTargetid(Long targetid) {
        this.targetid = targetid;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Long getCreatetm() {
        return createtm;
    }

    public void setCreatetm(Long createtm) {
        this.createtm = createtm;
    }

    public Long getUid() {

        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }
}
