package cn.idongjia.task.client.pojo;

import java.io.Serializable;

/**
 * Created by renxianlong on 16/2/26.
 */
public class Block implements Serializable{
    private long uid;
    private long blockid;
    private long createtm;

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }

    public long getBlockid() {
        return blockid;
    }

    public void setBlockid(long blockid) {
        this.blockid = blockid;
    }

    public long getCreatetm() {
        return createtm;
    }

    public void setCreatetm(long createtm) {
        this.createtm = createtm;
    }
}
