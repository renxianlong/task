package cn.idongjia.task.client.pojo;

import org.codehaus.jackson.annotate.JsonProperty;

public class File {
   @JsonProperty("TFS_FILE_NAME")
   private String tfsFileName;

   public String getTfsFileName() {
      return tfsFileName;
   }

   public void setTfsFileName(String tfsFileName) {
      this.tfsFileName = tfsFileName;
   }
}
