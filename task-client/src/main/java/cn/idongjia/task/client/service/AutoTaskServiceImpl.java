package cn.idongjia.task.client.service;

import cn.idongjia.article.pojo.TabRecommend;
import cn.idongjia.log.Log;
import cn.idongjia.log.LogFactory;
import cn.idongjia.task.client.support.LikeTask;
import cn.idongjia.task.client.support.RecommendTask;
import cn.idongjia.task.services.AutoTaskService;
import com.alibaba.dubbo.rpc.protocol.rest.support.ContentType;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

@Component("autoTaskService")
@Path("/tasks")
@Produces({ContentType.APPLICATION_JSON_UTF_8})
public class AutoTaskServiceImpl implements AutoTaskService {
    private static final Log LOGGER = LogFactory.getLog(AutoTaskServiceImpl.class);

    @Resource
    private LikeTask task;

    @Resource
    private RecommendTask recommendTask;

    @Override
    @PUT
    @Path("collect/{pid:\\d+}")
    public void addCollect(@PathParam("pid") final long pid) {
        task.addCollect(pid);
    }

    @Override
    @POST
    @Path("recommend")
    public void addRecommend(TabRecommend recommend) {
        recommendTask.addRecommend(recommend);
    }

    @Override
    @PUT
    @Path("recommend")
    public void updateRecommendWeight(TabRecommend recommend) {
        recommendTask.updateRecommendWeight(recommend);
    }

    @Override
    @DELETE
    @Path("recommend")
    public void cancelRecommend(TabRecommend recommend) {
        recommendTask.cancelRecommend(recommend);
    }
}