package cn.idongjia.task.client.mapper;

import cn.idongjia.common.query.BaseSearch;
import cn.idongjia.tianji.pojos.Evaluate;
import cn.idongjia.tianji.pojos.Order;
import cn.idongjia.tianji.pojos.OrderLog;
import cn.idongjia.tianji.pojos.Shipment;
import cn.idongjia.tianji.query.OrderSearch;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component("orderMapper")
public interface OrderMapper {

    @Insert("INSERT INTO kp_item_order (ooid,uid,aid,iid,suid,data,comment,quantity,realpay,createtm,updatetm,status,snapshot,otype,prepaytype) VALUES" +
            " (#{ooid},#{uid},#{aid},#{iid},#{suid},#{data},#{comment},#{quantity},#{realpay},#{createtm},#{updatetm},#{status},#{snapshot},#{otype},#{prepaytype})")
    @Options(useGeneratedKeys = true, keyProperty = "iid")
    int insert(Order order);

    @Select("SELECT * FROM kp_item_order WHERE ooid=#{id}")
    Order get(String id);

    @Select("SELECT * FROM kp_item_order WHERE ooid=#{id} FOR UPDATE")
    Order getAndLock(String id);

    @Select("<script>" +
            "SELECT * FROM kp_item_order " +
            " <where>" +
            " 1=1" +
            " <if test=\"iids != null\"> AND ooid IN " +
            " <foreach collection=\"array\" index=\"index\" order=\"order\" " +
            " open=\"(\" separator=\",\" close=\")\">" +
            " #{order} " +
            " </foreach></if>" +
            " </where>" +
            " <if test=\"search != null and search.orderBy != null\">" +
            " ORDER BY ${search.orderBy} " +
            " </if>" +
            " <if test=\"search != null and search.limit != null\">" +
            " LIMIT ${search.limit} " +
            " </if>" +
            " <if test=\"search != null and search.offset != null\">" +
            " OFFSET ${search.offset} " +
            " </if>" +
            "</script>")
    List<Order> batchGet(@Param("ooids") String[] ooids, @Param("search") BaseSearch search);

    @Update("<script>" +
            " UPDATE `kp_item_order` SET `ooid` = #{ooid}" +
            " <if test=\"uid != null\"> , `uid` = #{uid}</if>" +
            " <if test=\"aid != null\"> , `aid` = #{aid}</if>" +
            " <if test=\"iid != null\"> , `iid` = #{iid}</if>" +
            " <if test=\"data != null\"> , `data` = #{data}</if>" +
            " <if test=\"suid != null\"> , `suid` = #{suid}</if>" +
            " <if test=\"changeflag != null\"> , `changeflag` = #{changeflag}</if>" +
            " <if test=\"comment != null\"> , `comment` = #{comment}</if>" +
            " <if test=\"quantity != null\"> , `quantity` = #{quantity}</if>" +
            " <if test=\"realpay != null\"> , `realpay` = #{realpay}</if>" +
            " <if test=\"status != null\"> , `status` = #{status}</if>" +
            " <if test=\"refund != null\"> , `refund` = #{refund}</if>" +
            " <if test=\"otype != null\"> , `otype` = #{otype}</if>" +
            " <if test=\"prepaytype != null\"> , `prepaytype` = #{prepaytype}</if>" +
            " <if test=\"hidden != null\"> , `hidden` = #{hidden}</if>" +
            " <if test=\"snapshot != null\"> , `snapshot` = #{snapshot}</if>" +
            " <if test=\"createtm != null\"> , `createtm` = #{createtm}</if>" +
            " <if test=\"updatetm != null\"> , `updatetm` = #{updatetm}</if>" +
            " <if test=\"paytm != null\"> , `paytm` = #{paytm}</if>" +
            " <if test=\"sendtm != null\"> , `sendtm` = #{sendtm}</if>" +
            " <if test=\"receivetm != null\"> , `receivetm` = #{receivetm}</if>" +
            " <if test=\"delaytimes != null\"> , `delaytimes` = #{delaytimes}</if>" +
            " <if test=\"delaytime != null\"> , `delaytime` = #{delaytime}</if>" +
            " WHERE `ooid` = #{ooid}" +
            "</script>")
    int update(Order order);

    @Select("<script>" +
            "SELECT COUNT(*) total FROM kp_item_order " +
            " <where>" +
            " 1=1" +
            "  <if test=\"search!=null and search.ooid!=null\"> AND `ooid` = #{search.ooid}</if>" +
            "  <if test=\"search!=null and search.otype!=null\"> AND `otype` = #{search.otype}</if>" +
            "  <if test=\"search!=null and search.prepaytype!=null\"> AND `prepaytype` = #{search.prepaytype}</if>" +
            "  <if test=\"search!=null and search.uid!=null\"> AND o.`uid` = #{search.uid}</if>" +
            "  <if test=\"search!=null and search.suid!=null\"> AND `suid` = #{search.suid}</if>" +
            "  <if test=\"search!=null and search.iid!=null\"> AND `iid` = #{search.iid}</if>" +
            "  <if test=\"search!=null and search.changeflag != null\"> AND `changeflag` = #{search.changeflag}</if>" +
            "  <if test=\"search!=null and search.status!=null\"> AND o.`status` = #{search.status}</if>" +
            "  <if test=\"search!=null and search.hidden!=null\"> AND `hidden` = #{search.hidden}</if>" +
            "  <if test=\"search!=null and search.startd!=null\"> AND o.`createtm` &gt; #{search.startd}</if>" +
            "  <if test=\"search!=null and search.endd!=null\"> AND o.`createtm` &lt; #{search.endd}</if>" +
            "  <if test=\"search!=null and search.statusRange!=null\"> AND o.`status` ${search.statusRange}</if>" +
            "  <if test=\"search!=null and search.hiddenRange!=null\"> AND `hidden` ${search.hiddenRange}</if>" +
            "  <if test=\"search!=null and search.refundRange!=null\"> AND `refund` ${search.refundRange}</if>" +
            " </where>" +
            "</script>")
    int count(@Param("search") OrderSearch search);

    @Select("<script>" +
            "SELECT o.*,u.username FROM kp_item_order o LEFT JOIN kp_user u ON " +
            "   <choose>" +
            "   <when test=\"search!=null and search.needUsername!=null\">" +
            "   o.uid=u.uid" +
            "   </when>" +
            "   <otherwise>" +
            "   o.suid=u.uid" +
            "   </otherwise>" +
            "   </choose>" +
            " <where>" +
            " 1=1" +
            "  <if test=\"search!=null and search.ooid!=null\"> AND `ooid` = #{search.ooid}</if>" +
            "  <if test=\"search!=null and search.uid!=null\"> AND o.`uid` = #{search.uid}</if>" +
            "  <if test=\"search!=null and search.suid!=null\"> AND `suid` = #{search.suid}</if>" +
            "  <if test=\"search!=null and search.iid!=null\"> AND `iid` = #{search.iid}</if>" +
            "  <if test=\"search!=null and search.otype!=null\"> AND `otype` = #{search.otype}</if>" +
            "  <if test=\"search!=null and search.prepaytype!=null\"> AND `prepaytype` = #{search.prepaytype}</if>" +
            "  <if test=\"search!=null and search.changeflag != null\"> AND `changeflag` = #{search.changeflag}</if>" +
            "  <if test=\"search!=null and search.status!=null\"> AND o.`status` = #{search.status}</if>" +
            "  <if test=\"search!=null and search.hidden!=null\"> AND `hidden` = #{search.hidden}</if>" +
            "  <if test=\"search!=null and search.startd!=null\"> AND o.`createtm` &gt; #{search.startd}</if>" +
            "  <if test=\"search!=null and search.endd!=null\"> AND o.`createtm` &lt; #{search.endd}</if>" +
            "  <if test=\"search!=null and search.statusRange!=null\"> AND o.`status` ${search.statusRange}</if>" +
            "  <if test=\"search!=null and search.hiddenRange!=null\"> AND `hidden` ${search.hiddenRange}</if>" +
            "  <if test=\"search!=null and search.refundRange!=null\"> AND `refund` ${search.refundRange}</if>" +
            " </where>" +
            " <if test=\"search != null and search.orderBy != null\">" +
            " ORDER BY ${search.orderBy} " +
            " </if>" +
            " <if test=\"search != null and search.limit != null\">" +
            " LIMIT ${search.limit} " +
            " </if>" +
            " <if test=\"search != null and search.offset != null\">" +
            " OFFSET ${search.offset} " +
            " </if>" +
            "</script>")
    List<Order> getAll(@Param("search") OrderSearch search);

    @Select("<script>" +
            "SELECT o.*,u.username,c.username craftsman,ct.name category,pc.name parentCategory" +
            " FROM kp_item_order o " +
            "  LEFT JOIN kp_user c ON c.uid=o.suid " +
            "  LEFT JOIN kp_user u ON u.uid=o.uid" +
            "  LEFT JOIN kp_item i ON i.iid=o.iid" +
            "  LEFT JOIN kp_item_category ct on ct.icid=i.category" +
            "  LEFT JOIN kp_item_category pc on pc.icid=ct.parentid" +
            " <where>" +
            " 1=1" +
            "  <if test=\"search!=null and search.ooid!=null\"> AND `ooid` = #{search.ooid}</if>" +
            "  <if test=\"search!=null and search.cid!=null\">" +
            "    AND (i.`category` = #{search.cid} or `ct`.`parentid` = #{search.cid})" +
            "  </if>" +
            "  <if test=\"search!=null and search.uid!=null\"> AND o.`uid` = #{search.uid}</if>" +
            "  <if test=\"search!=null and search.suid!=null\"> AND `suid` = #{search.suid}</if>" +
            "  <if test=\"search!=null and search.iid!=null\"> AND o.`iid` = #{search.iid}</if>" +
            "  <if test=\"search!=null and search.changeflag != null\"> AND `changeflag` = #{search.changeflag}</if>" +
            "  <if test=\"search!=null and search.status!=null\"> AND o.`status` = #{search.status}</if>" +
            "  <if test=\"search!=null and search.refund!=null\"> AND o.`refund` = #{search.refund}</if>" +
            "  <if test=\"search!=null and search.otype!=null\"> AND o.`otype` = #{search.otype}</if>" +
            "  <if test=\"search!=null and search.prepaytype!=null\"> AND o.`prepaytype` = #{search.prepaytype}</if>" +
            "  <if test=\"search!=null and search.hidden!=null\"> AND `hidden` = #{search.hidden}</if>" +
            "  <if test=\"search!=null and search.priceMin != null\"> AND o.`realpay` &gt;= #{search.priceMin}</if>" +
            "  <if test=\"search!=null and search.priceMax != null\"> AND o.`realpay` &lt;= #{search.priceMax}</if>" +
            "  <if test=\"search!=null and search.sellMin != null\"> AND o.`quantity` &gt;= #{search.sellMin}</if>" +
            "  <if test=\"search!=null and search.sellMax != null\"> AND o.`quantity` &lt;= #{search.sellMax}</if>" +
            "  <if test=\"search!=null and search.startd!=null\"> AND o.`createtm` &gt; #{search.startd}</if>" +
            "  <if test=\"search!=null and search.endd!=null\"> AND o.`createtm` &lt; #{search.endd}</if>" +
            "  <if test=\"search!=null and search.statusRange!=null\"> AND o.`status` ${search.statusRange}</if>" +
            "  <if test=\"search!=null and search.title!=null\"> AND i.`title` LIKE #{search.title}</if>" +
            "  <if test=\"search!=null and search.type!=null\"> AND i.`type` = #{search.type}</if>" +
            "  <if test=\"search!=null and search.craftsman!=null\"> AND c.`username` LIKE #{search.craftsman}</if>" +
            "  <if test=\"search!=null and search.username!=null\"> AND u.`username` LIKE #{search.username}</if>" +
            " </where>" +
            " <if test=\"search != null and search.orderBy != null\">" +
            " ORDER BY ${search.orderBy} " +
            " </if>" +
            " <if test=\"search != null and search.limit != null\">" +
            " LIMIT ${search.limit} " +
            " </if>" +
            " <if test=\"search != null and search.offset != null\">" +
            " OFFSET ${search.offset} " +
            " </if>" +
            "</script>")
    List<Order> search(@Param("search") OrderSearch search);

    @Select("<script>" +
            "SELECT count(o.ooid) FROM kp_item_order o " +
            "  LEFT JOIN kp_user c ON c.uid=o.suid " +
            "  LEFT JOIN kp_user u ON u.uid=o.uid" +
            "  LEFT JOIN kp_item i ON i.iid=o.iid" +
            "  LEFT JOIN kp_item_category ct on ct.icid=i.category" +
            "  LEFT JOIN kp_item_category pc on pc.icid=ct.parentid" +
            " <where>" +
            " 1=1" +
            "  <if test=\"search!=null and search.ooid!=null\"> AND `ooid` = #{search.ooid}</if>" +
            "  <if test=\"search!=null and search.cid!=null\">" +
            "    AND (i.`category` = #{search.cid} or `ct`.`parentid` = #{search.cid})" +
            "  </if>" +
            "  <if test=\"search!=null and search.uid!=null\"> AND o.`uid` = #{search.uid}</if>" +
            "  <if test=\"search!=null and search.suid!=null\"> AND `suid` = #{search.suid}</if>" +
            "  <if test=\"search!=null and search.iid!=null\"> AND o.`iid` = #{search.iid}</if>" +
            "  <if test=\"search!=null and search.changeflag != null\"> AND `changeflag` = #{search.changeflag}</if>" +
            "  <if test=\"search!=null and search.status!=null\"> AND o.`status` = #{search.status}</if>" +
            "  <if test=\"search!=null and search.refund!=null\"> AND o.`refund` = #{search.refund}</if>" +
            "  <if test=\"search!=null and search.otype!=null\"> AND o.`otype` = #{search.otype}</if>" +
            "  <if test=\"search!=null and search.prepaytype!=null\"> AND o.`prepaytype` = #{search.prepaytype}</if>" +
            "  <if test=\"search!=null and search.hidden!=null\"> AND `hidden` = #{search.hidden}</if>" +
            "  <if test=\"search!=null and search.priceMin != null\"> AND o.`realpay` &gt;= #{search.priceMin}</if>" +
            "  <if test=\"search!=null and search.priceMax != null\"> AND o.`realpay` &lt;= #{search.priceMax}</if>" +
            "  <if test=\"search!=null and search.sellMin != null\"> AND o.`quantity` &gt;= #{search.sellMin}</if>" +
            "  <if test=\"search!=null and search.sellMax != null\"> AND o.`quantity` &lt;= #{search.sellMax}</if>" +
            "  <if test=\"search!=null and search.startd!=null\"> AND o.`createtm` &gt; #{search.startd}</if>" +
            "  <if test=\"search!=null and search.endd!=null\"> AND o.`createtm` &lt; #{search.endd}</if>" +
            "  <if test=\"search!=null and search.statusRange!=null\"> AND o.`status` ${search.statusRange}</if>" +
            "  <if test=\"search!=null and search.title!=null\"> AND i.`title` LIKE #{search.title}</if>" +
            "  <if test=\"search!=null and search.type!=null\"> AND i.`type` = #{search.type}</if>" +
            "  <if test=\"search!=null and search.craftsman!=null\"> AND c.`username` LIKE #{search.craftsman}</if>" +
            "  <if test=\"search!=null and search.username!=null\"> AND u.`username` LIKE #{search.username}</if>" +
            " </where>" +
            "</script>")
    int searchCount(@Param("search") OrderSearch search);

    @Update("UPDATE kp_item_order SET status=-1 WHERE ooid=#{ooid}")
    int delete(String ooid);

    @Update("UPDATE kp_item_order SET status=0 WHERE ooid=#{ooid}")
    int close(String ooid);

    @Insert("INSERT INTO kp_item_order_log (ooid,createtm,type,adminflag,uid,data) VALUES" +
            " (#{ooid},#{createtm},#{type},#{adminflag},#{uid},#{data})")
    @Options(useGeneratedKeys = true, keyProperty = "iolid")
    int writeLog(OrderLog log);

    @Select("<script>" +
            "SELECT * FROM kp_item_order_log " +
            " <where>" +
            " 1=1" +
            "  <if test=\"ooid != null\"> AND `ooid` = #{ooid}</if>" +
            " </where>" +
            " <if test=\"search != null and search.orderBy != null\">" +
            " ORDER BY ${search.orderBy} " +
            " </if>" +
            " <if test=\"search != null and search.limit != null\">" +
            " LIMIT ${search.limit} " +
            " </if>" +
            " <if test=\"search != null and search.offset != null\">" +
            " OFFSET ${search.offset} " +
            " </if>" +
            "</script>")
    List<OrderLog> getLogs(@Param("ooid") String ooid,
                           @Param("search") BaseSearch search);

    @Select("SELECT * FROM kp_item_order_log WHERE iolid=#{id}")
    OrderLog getLog(Long id);

    @Select("SELECT o.*,i.pid,i.uid iuid,i.status istatus,i.title,i.pictures,i.category,i.desc,i.services,i.evlcnt," +
            "i.stock,i.updatetm iupdatetm FROM kp_item_order o LEFT JOIN kp_item i ON o.iid=i.iid WHERE o.ooid=#{ooid}")
    Map getOrderAndItem(String ooid);

    @Insert("INSERT INTO kp_item_order_evaluate (ooid,uid,iid,content,createtm) VALUES" +
            " (#{ooid},#{uid},#{iid},#{content},#{createtm})")
    @Options(useGeneratedKeys = true, keyProperty = "eid")
    int evaluate(Evaluate evaluate);

    @Update("REPLACE INTO kp_shipment (ooid,cid,no,address,createtm) VALUES" +
            " (#{ooid},#{cid},#{no},#{address},#{createtm})")
    int ship(Shipment shipment);

    @Select("SELECT * FROM kp_shipment WHERE ooid=#{ooid}")
    int getShip(String ooid);

    @Select("SELECT prepay FROM kp_payment WHERE ooid=#{ooid}")
    String getPrepay(String ooid);

    @Update("UPDATE kp_item_order SET delaytimes=#{delaytimes} WHERE ooid=#{ooid}")
    int delay(@Param("delaytimes") Integer delaytimes, @Param("ooid") String ooid);

    @Update("UPDATE kp_item_order SET delaytime=#{delaytime} WHERE ooid=#{ooid}")
    int sumdelay(@Param("delaytime") Long delaytime, @Param("ooid") String ooid);
}
