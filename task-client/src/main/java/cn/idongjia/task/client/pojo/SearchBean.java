package cn.idongjia.task.client.pojo;

/**
 * Created by leedongwei on 15/12/15.
 package cn.idongjia.tianji.pojo;

 /**
 * Created by leedongwei on 15/12/14.
 */
public class SearchBean {

    long id;
    String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getId() {

        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
