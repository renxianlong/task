package cn.idongjia.task.client.pojo;

public class Cashout {
    private Long cid;
    private Long createtm;
    private Long uid;
    private Double cashoutAmount;
    private Double accountBalance;
    private Long sbid;
    private Integer status;
    private String data;

    public Long getCid() {
        return cid;
    }

    public void setCid(Long cid) {
        this.cid = cid;
    }

    public Long getCreatetm() {
        return createtm;
    }

    public void setCreatetm(Long createtm) {
        this.createtm = createtm;
    }

    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }

    public Double getCashoutAmount() {
        return cashoutAmount;
    }

    public void setCashoutAmount(Double cashoutAmount) {
        this.cashoutAmount = cashoutAmount;
    }

    public Double getAccountBalance() {
        return accountBalance;
    }

    public void setAccountBalance(Double accountBalance) {
        this.accountBalance = accountBalance;
    }

    public Long getSbid() {
        return sbid;
    }

    public void setSbid(Long sbid) {
        this.sbid = sbid;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
