package cn.idongjia.task.client.mapper;

import cn.idongjia.tianji.pojos.Bank;
import cn.idongjia.tianji.query.BankSearch;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

@Component("bankMapper")
public interface BankMapper {
    @Insert("INSERT INTO kp_bank (name,pic,status,weight) VALUES" +
            " (#{name}, #{pic}, #{status}, #{weight})")
    @Options(useGeneratedKeys = true, keyProperty = "bid")
    int insert(Bank bank);

    @Select("SELECT * FROM kp_bank WHERE bid=#{bid}")
    Bank getById(Long bid);

    @Update("<script>" +
            "UPDATE `kp_bank` SET `bid` = #{bid}" +
            " <if test=\"name != null\"> , `name` = #{name}</if>" +
            " <if test=\"pic != null\"> , `pic` = #{pic}</if>" +
            " <if test=\"weight != null\"> , `weight` = #{weight}</if>" +
            " <if test=\"status != null\"> , `status` = #{status}</if>" +
            " WHERE `bid` = #{bid}" +
            "</script>")
    int update(Bank bank);

    @Select("<script>" +
            "SELECT COUNT(*) FROM kp_bank " +
            " <where>" +
            " 1=1" +
            "  <if test=\"search != null and search.status != null\">" +
            "    AND `status` = #{search.status}" +
            "  </if>" +
            "  <if test=\"search != null and search.weight != null\">" +
            "    AND `weight` = #{search.weight}" +
            "  </if>" +
            " </where>" +
            "</script>")
    int count(@Param("search") BankSearch search);

    @Select("<script>" +
            "SELECT * FROM kp_bank " +
            " <where>" +
            " 1=1" +
            "  <if test=\"search != null and search.status != null\">" +
            "    AND `status` = #{search.status}" +
            "  </if>" +
            "  <if test=\"search != null and search.weight != null\">" +
            "    AND `weight` = #{search.weight}" +
            "  </if>" +
            " </where>" +
            " <if test=\"search != null and search.orderBy != null\">" +
            " ORDER BY ${search.orderBy} " +
            " </if>" +
            " <if test=\"search != null and search.limit != null\">" +
            " LIMIT ${search.limit} " +
            " </if>" +
            " <if test=\"search != null and search.offset != null\">" +
            " OFFSET ${search.offset} " +
            " </if>" +
            "</script>")
    List<Bank> getAll(@Param("search") BankSearch search);

    @Update("UPDATE kp_bank SET status=-1 WHERE bid=#{bid}")
    int delete(Long bid);

}
