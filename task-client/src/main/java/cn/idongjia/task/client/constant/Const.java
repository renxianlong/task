package cn.idongjia.task.client.constant;

import javax.ws.rs.core.MediaType;

public class Const {
    // adminflag
    public static final int ADMINFLAG_USER = 0;
    public static final int ADMINFLAG_ADMIN = 1;
    public static final int ADMINFLAG_SYSTEM = 2;

    // autolike
    public static final String AUTO_LIKE_POST = "autoLikeList";
    public static final String AUTO_COLLECT_POST = "autoCollectList";
    public static final String USER_MAJIA = "user_waistcoat_cache";

    public static final String ORDER_TASK_KEY = "Orders.tasks";

    public static final String ORDER_NEED_PAY = "orderNeedPay.";

    public static final String NOTIFY_TITLE = "来自东家的消息";

    public static final int ANDROID_INDEX = 0;
    public static final int IOS_INDEX = 1;

    // unprocessed ship type
    public static final int UNSHIPED = 0;
    public static final int UNALLOW = 1;
    public static final String ORDER_UNSHIPED_REMIND = "ORDER_UNSHIPED_REMIND";
    public static final String ORDER_UNSHIPED = "ORDER_UNSHIPED";
    public static final String ORDER_UNCONFIRMED = "ORDER_UNCONFIRMED";
    public static final String ORDER_NOT_CONFIRMED = "ORDER_NOT_CONFIRMED";
    public static final String REFUND_UNHANDLE = "REFUND_UNHANDLE";
    public static final String REFUND_UNCONFIRM = "REFUND_UNCONFIRM";

    // returngoods timer type
    public static final String RETURNGOOD_UNALLOW_TIMEOUT = "RETURNGOOD_UNALLOW_TIMEOUT";
    public static final String RETURNGOOD_UNALLOW_ALERT1 = "RETURNGOOD_UNALLOW_ALERT1";
    public static final String RETURNGOOD_UNALLOW_ALERT2 = "RETURNGOOD_UNALLOW_ALERT2";
    public static final String RETURNGOOD_UNSEND_ALERT = "RETURNGOOD_UNSEND_ALERT";
    public static final String RETURNGOOD_UNSEND_TIMEOUT = "RETURNGOOD_UNSEND_TIMEOUT";

    public static final String UTF8 = "UTF-8";
    public static final String GLOBAL_MEDIATYPE = MediaType.APPLICATION_JSON + ";charset=" + UTF8;
    public static final String MEDIATYPE_URLENCODE =
            MediaType.APPLICATION_FORM_URLENCODED + ";charset=" + UTF8;

    public static final String REMIND_SHIP_ITEM = "亲爱的手艺人%s，48小时过去了，" +
            "您还没有在东家按下发货按钮哦！买家%s正在焦心的等待您的匠心之作，速速去发货吧！";
    public static final String ORDER_UNCONFIRM_DESC = "您购买的“%s”，将于%d天后自动收货，请尽快查看" +
            "，如有问题请尽快联系客服";
    public static final String REFUND_UNHANDLE_DESC = "您还未处理买家%s提交的退款申请，" +
            "该申请将于%d天后自动同意，请尽快查看";
    public static final String RETURN_UNHANDLE_DESC = "买家%s退回的“%s”，将于%d天后自动收货，请尽快查看，" +
            "如有问题请尽快联系客服";

    public static final String AUCTION_CRAFTSMAN = "craftsman";
    public static final String AUCTION_NEXT = "next";
    public static final String AUCTION_INTERVAL = "interval";

    public static final int AUCTION_OK = 0;

    //search task
    public static final String SEARCH_TASK_CRAFTSMANKEY = "Search.tasks.craftsman";
    public static final String SEARCH_TASK_ITEMMANKEY = "Search.tasks.item";

    //退货相关接口描述信息
    public static final String RETURNGOOD_UNALLOW_ALERT1_DESC = "亲爱的手艺人%s，24小时过去了，" +
            "您还没有处理买家%s提出的退货退款申请，请尽快处理";
    public static final String RETURNGOOD_UNALLOW_ALERT2_DESC = "亲爱的手艺人%s，48小时过去了，" +
            "您还没有处理买家%s提出的退货退款申请，请尽快处理";
    public static final String RETURNGOOD_UNALLOW_TIMEOUT_DESC = "因超时未处理，东家客服已介入";
    public static final String RETURNGOOD_ALLOW = "您购买的“%s”，匠人已同意退货退款，请尽快将退货发出";
    public static final String REFUND_CONFIRM = "您购买的“%s”，匠人同意退款，请注意查收退回款项";
    public static final String RETURNGOOD_CONFIRM = "您退回的“%s”，匠人已确认收货，请注意查收退回款项，" +
            "如有疑问请联系客服";
    public static final String REFUND_APPLY = "您的商品“%s”，买家已申请退款，请尽快处理";
    public static final String RETURNGOOD_APPLY = "您的商品“%s”，买家已申请退货退款，请尽快处理";
    public static final String RETURNGOOD_DISALLOW = "您购买的“%s”，匠人已拒绝退货退款申请，" +
            "如有疑问请联系客服";
    public static final String RETURNGOOD_SEND = "您的商品“%s”，买家已经将退货发出，请尽快确认收货";
    public static final String RETURNGOOD_UNSEND_DESC = "您购买的“%s”，匠人已经同意退货退款，" +
            "如您一直未发货，该退货申请将于%d天后自动取消";
    public static final String RETURNGOOD_UNSEND_TIMEOUT_DESC = "您购买的“%s”，匠人已经同意退货退款，" +
            "因您一直未将退货发出，该退货申请已自动取消";
    public static final String REFUND_CONFIRM_TIMEOUT_CANCEL_24 = "您已取消退款申请，" +
            "剩余24小时自动确认收货";
    public static final String REFUND_CONFIRM_TIMEOUT_REJECT_24 = "匠人拒绝退款申请，" +
            "剩余24小时自动确认收货，若有疑问请前往退款详情页进行申诉";
    public static final String REFUND_CONFIRM_TIMEOUT_DISALLOW_24 = "匠人拒绝退款申请，" +
            "剩余24小时自动确认收货，若有疑问请前往退款详情页进行申诉";
    public static final String REFUND_REJECT_DESC = "您购买的“%s”，匠人拒绝退款，如有疑问请联系客服";
    public static final String RETURNGOOD_REJECT_DESC = "您购买的“%s”，匠人已拒绝退货退款，" +
            "如有疑问请联系客服";

    // query param
    public static final String ORDERBY = "orderby";
    public static final String PAGE = "page";
    public static final String LIMIT = "perpage";

    // sms event
    public static final String SMS_EVENT = "sms_event";

    // address length
    public static final int ADDRESS_STR_LENGTH = 60;
}
