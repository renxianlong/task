package cn.idongjia.task.client.mapper;

import cn.idongjia.tianji.pojos.AfterSale;
import cn.idongjia.tianji.query.AfterSaleSearch;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

@Component("afterSaleMapper")
public interface AfterSaleMapper {
    @Insert("INSERT INTO kp_item_service (`name`,`desc`,`status`,`icon`,createtm) VALUES" +
            " (#{name},#{desc},#{status},#{icon},#{createtm})")
    @Options(useGeneratedKeys = true, keyProperty = "isid")
    int insert(AfterSale afterSale);

    @Select("SELECT * FROM kp_item_service WHERE isid=#{id}")
    AfterSale getById(Long id);

    @Update("<script>" +
            "UPDATE `kp_item_service` SET `isid` = #{isid}" +
            " <if test=\"name != null\"> , `name` = #{name}</if>" +
            " <if test=\"desc != null\"> , `desc` = #{desc}</if>" +
            " <if test=\"status != null\"> , `status` = #{status}</if>" +
            " <if test=\"icon != null\"> , `icon` = #{icon}</if>" +
            " <if test=\"createtm != null\"> , `createtm` = #{createtm}</if>" +
            " WHERE `isid` = #{isid}" +
            "</script>")
    int update(AfterSale afterSale);

    @Select("<script>" +
            "SELECT COUNT(*) total FROM kp_item_service " +
            " <where>" +
            " 1=1" +
            "  <if test=\"search!=null and search.status!=null\">" +
            "    AND `status` = #{search.status}" +
            "  </if>" +
            "  <if test=\"search!=null and search.name!=null\">" +
            "    AND `name` LIKE %#{search.name}%" +
            "  </if>" +
            "  <if test=\"search!=null and search.desc != null\">" +
            "    AND `desc` LIKE %#{search.desc}%" +
            "  </if>" +
            " </where>" +
            "</script>")
    int count(@Param("search") AfterSaleSearch search);

    @Select("<script>" +
            "SELECT * FROM kp_item_service " +
            " <where>" +
            " 1=1" +
            "  <if test=\"search!=null and search.status!=null\">" +
            "    AND `status` = #{search.status}" +
            "  </if>" +
            "  <if test=\"search!=null and search.name!=null\">" +
            "    AND `name` LIKE %#{search.name}%" +
            "  </if>" +
            "  <if test=\"search!=null and search.desc != null\">" +
            "    AND `desc` LIKE %#{search.desc}%" +
            "  </if>" +
            " </where>" +
            " <if test=\"search!=null and search.orderBy!=null\">" +
            " ORDER BY ${search.orderBy} " +
            " </if>" +
            " <if test=\"search!=null and search.limit!=null\">" +
            " LIMIT ${search.limit} " +
            " </if>" +
            " <if test=\"search!=null and search.offset!=null\">" +
            " OFFSET ${search.offset} " +
            " </if>" +
            "</script>")
    List<AfterSale> getAll(@Param("search") AfterSaleSearch search);

    @Update("UPDATE kp_item_service SET status=-1 WHERE isid=#{id}")
    int delete(Long id);
}
