package cn.idongjia.task.client.mapper;

import cn.idongjia.tianji.pojos.Category;
import cn.idongjia.tianji.query.CategorySearch;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

@Component("categoryMapper")
public interface CategoryMapper {

    @Insert("INSERT INTO kp_item_category (name,createtm,parentid,status,weight) VALUES" +
            " (#{name}, #{createtm}, #{parentid}, #{status},#{weight})")
    @Options(useGeneratedKeys = true, keyProperty = "icid")
    int insert(Category category);

    @Select("SELECT * FROM kp_item_category WHERE icid=#{icid}")
    Category getById(Long icid);

    @Update("<script>" +
            "UPDATE `kp_item_category` SET `icid` = #{icid}" +
            " <if test=\"name != null\"> , `name` = #{name}</if>" +
            " <if test=\"parentid != null\"> , `parentid` = #{parentid}</if>" +
            " <if test=\"weight != null\"> , `weight` = #{weight}</if>" +
            " <if test=\"status != null\"> , `status` = #{status}</if>" +
            " WHERE `icid` = #{icid}" +
            "</script>")
    int update(Category category);

    @Select("<script>" +
            "SELECT COUNT(*) total FROM kp_item_category " +
            " <where>" +
            " 1=1" +
            "  <if test=\"search != null and search.icid != null\">" +
            "    AND `icid` = #{search.icid}" +
            "  </if>" +
            "  <if test=\"search != null and search.status != null\">" +
            "    AND `status` = #{search.status}" +
            "  </if>" +
            "  <if test=\"search != null and search.parentid != null\">" +
            "    AND `parentid` = #{search.parentid}" +
            "  </if>" +
            "  <if test=\"search != null and search.name!=null\">" +
            "    AND `name` LIKE #{search.name}" +
            "  </if>" +
            " </where>" +
            "</script>")
    int count(@Param("search") CategorySearch search);

    @Select("<script>" +
            "SELECT * FROM kp_item_category " +
            " <where>" +
            " 1=1" +
            "  <if test=\"search != null and search.icid != null\">" +
            "    AND `icid` = #{search.icid}" +
            "  </if>" +
            "  <if test=\"search != null and search.status != null\">" +
            "    AND `status` = #{search.status}" +
            "  </if>" +
            "  <if test=\"search != null and search.parentid != null\">" +
            "    AND `parentid` = #{search.parentid}" +
            "  </if>" +
            "  <if test=\"search != null and search.name!=null\">" +
            "    AND `name` LIKE #{search.name}" +
            "  </if>" +
            " </where>" +
            " <if test=\"search != null and search.orderBy != null\">" +
            " ORDER BY ${search.orderBy} " +
            " </if>" +
            " <if test=\"search != null and search.limit != null\">" +
            " LIMIT ${search.limit} " +
            " </if>" +
            " <if test=\"search != null and search.offset != null\">" +
            " OFFSET ${search.offset} " +
            " </if>" +
            "</script>")
    List<Category> getAll(@Param("search") CategorySearch search);

    @Select("<script>" +
            "SELECT c.*,p.name parent FROM kp_item_category c " +
            " left join kp_item_category p on p.icid=c.parentid" +
            " <where>" +
            " 1=1" +
            "  <if test=\"search != null and search.icid != null\">" +
            "    AND c.`icid` = #{search.icid}" +
            "  </if>" +
            "  <if test=\"search != null and search.status != null\">" +
            "    AND c.`status` = #{search.status}" +
            "  </if>" +
            "  <if test=\"search != null and search.parentid != null\">" +
            "    AND c.`parentid` = #{search.parentid}" +
            "  </if>" +
            "  <if test=\"search!=null and search.parent!=null\">" +
            "   <if test=\"search.parent==0\">" +
            "   AND c.parentid=0" +
            "   </if>" +
            "   <if test=\"search.parent==1\">" +
            "   AND c.parentid &gt; 0" +
            "   </if>" +
            "  </if>" +
            " <if test=\"search != null and search.name!=null\">" +
            "    AND c.`name` LIKE #{search.name}" +
            "  </if>" +
            " <if test=\"search != null and search.category!=null\">" +
            "   AND (c.`name` LIKE #{search.category} or p.`name` LIKE #{search.category})" +
            " </if>" +
            " </where>" +
            " <if test=\"search != null and search.orderBy != null\">" +
            " ORDER BY ${search.orderBy} " +
            " </if>" +
            " <if test=\"search != null and search.limit != null\">" +
            " LIMIT ${search.limit} " +
            " </if>" +
            " <if test=\"search != null and search.offset != null\">" +
            " OFFSET ${search.offset} " +
            " </if>" +
            "</script>")
    List<Category> search(@Param("search") CategorySearch search);

    @Update("UPDATE kp_item_category SET status=-1 WHERE icid=#{id}")
    int delete(Long id);
}
