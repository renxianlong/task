package cn.idongjia.task.provider.init;

import cn.idongjia.task.common.command.CommandUtil;
import cn.idongjia.task.common.siginal.ShutDownHandler;
import cn.idongjia.task.common.task.ExcuteType;
import cn.idongjia.task.common.task.Task;
import cn.idongjia.task.provider.config.TaskProviderConfig;
import com.lts.core.commons.utils.StringUtils;
import com.lts.core.domain.Job;
import com.lts.jobclient.JobClient;
import com.lts.jobclient.RetryJobClient;
import com.lts.jobclient.domain.Response;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.PosixParser;
import org.apache.log4j.Logger;
import org.apache.log4j.net.SyslogAppender;
import sun.misc.Signal;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Random;

/**
 * Created by renxianlong on 16/3/30.
 */
public class DefaultTaskProvider {

     public static SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh24:mm:ss:SSS");

     public static String ROOT_PATH = System.getProperty("task.home");

     public static String CONF_FILE = ROOT_PATH + "/conf/task-provider.properties";

     public static Logger logger = Logger.getLogger(DefaultTaskProvider.class);

     public static void main(String[] args) {

         long beginTm = System.currentTimeMillis();
         System.out.print(format.format(beginTm) + ":系统开始启动");
         logger.info(format.format(beginTm) + ":系统开始启动");
         //解析命令
         CommandLine commandLine =
                 CommandUtil.parseCmdLine("taskcenter", args, buildCommandlineOptions(new Options()),
                         new PosixParser());

         TaskProviderConfig config = new TaskProviderConfig();
         TaskProvider provider = new TaskProvider();

         if (new File(CONF_FILE).exists()) {
             CommandUtil.propertiesFileToObject(CONF_FILE, config);
         }

         if (commandLine.hasOption("p")) {
             CommandUtil.printObjectProperties(null, config);
             return;
         }
         if (commandLine.hasOption("c")) {
             String configFile = commandLine.getOptionValue("c");
             if (null == configFile) {
                 existByError("找不到配置文件,系统异常终止");
             }
             CommandUtil.propertiesFileToObject(configFile, config);
         }

         provider.initProvider(config);
         //启动
         provider.start();

         ShutDownHandler shutDownHandler = new ShutDownHandler(provider);
         Signal.handle(new Signal("TERM"), shutDownHandler);

         long endTm = System.currentTimeMillis();
         long elapse = endTm - beginTm;
         logger.info(format.format(endTm) + ":系统启动成功,耗时:" + elapse);
         System.out.print(format.format(endTm) + ":系统启动成功,耗时:" + elapse);


         long times = 100000;
         //测试provider -t times 数量
         if (commandLine.hasOption("t")) {
             times = Long.parseLong(commandLine.getOptionValue("t"));
         }
         logger.debug("收到测试任务" + times + "条");
         beginTm = System.currentTimeMillis();
         logger.debug("开始执行");
         Random rm = new Random(1000000);
         for(long i=0;i< times;i++){
             Task task = new Task();
             task.setTaskType("xingnengceshitask");
             task.setTaskId(System.currentTimeMillis() + "" + rm.nextInt());
             task.setExuterName("LogTaskExcuterGroup");
             task.setExtParam("logfile","task4");
             provider.submitTask(task);
         }
         endTm = System.currentTimeMillis();
         logger.debug("执行结束,耗时" + (endTm-beginTm) + "ms");
         provider.stop();
     }


    public static Options buildCommandlineOptions(final Options options) {
        Option opt = new Option("c", "configFile", true, "taskcenter config properties file");
        opt.setRequired(false);
        options.addOption(opt);

        opt = new Option("p", "printConfigItem", false, "Print all config item");
        opt.setRequired(false);
        options.addOption(opt);

        return options;
    }

    public static void existByError(String errorMsg) {
        System.out.print(errorMsg);
        System.exit(-1);
    }
}
