package cn.idongjia.task.provider.common;

import cn.idongjia.task.common.task.TaskResult;

/**
 * Created by renxianlong on 16/4/7.
 */
public interface TaskFeedBackHandler {
    void onFeedBack(TaskResult taskResult);
}
