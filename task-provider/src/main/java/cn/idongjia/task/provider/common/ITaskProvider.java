package cn.idongjia.task.provider.common;

import cn.idongjia.task.common.task.Task;

/**
 * Created by renxianlong on 16/4/20.
 */
public interface ITaskProvider {
    /*
       提交任务
    */
    void submitTask(Task task);

    /*
        取消任务
     */
    void cancelTask(String taskId);

    /*
        更新任务
     */
    void updateTask(Task task);
}
