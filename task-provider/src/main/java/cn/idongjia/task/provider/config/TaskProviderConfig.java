package cn.idongjia.task.provider.config;

import cn.idongjia.task.provider.common.TaskFeedBackHandler;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by renxianlong on 16/4/5.
 */
public class TaskProviderConfig {
    //集群名称,各集群节点需要保证集群名称一致,以保证注册中心将整个集群统一管理
    private String clusterName = "task_cluster";
    //注册中心地址,即ZK地址
    private String registAddr = "zookeeper://127.0.0.1:2181";
    //设置业务日志记录, 可选值: mongo, mysql,console, 推荐使用 mongo
    private String TaskProviderGroup = "DefaultTaskProvicerGroup";
    //任务发布者名字
    private String TaskProviderName = "DefaultTaskProvicerName";
    //回调配置
    private Map<String,TaskFeedBackHandler> handlerMap = new ConcurrentHashMap<>();

    public String getClusterName() {
        return clusterName;
    }

    public void setClusterName(String clusterName) {
        this.clusterName = clusterName;
    }

    public String getRegistAddr() {
        return registAddr;
    }

    public void setRegistAddr(String registAddr) {
        this.registAddr = registAddr;
    }

    public String getTaskProviderGroup() {
        return TaskProviderGroup;
    }

    public void setTaskProviderGroup(String taskProviderGroup) {
        TaskProviderGroup = taskProviderGroup;
    }

    public String getTaskProviderName() {
        return TaskProviderName;
    }

    public void setTaskProviderName(String taskProviderName)
    {
        TaskProviderName = taskProviderName;
    }

    public Map<String, TaskFeedBackHandler> getHandlerMap() {
        return handlerMap;
    }

    public void setHandlerMap(Map<String, TaskFeedBackHandler> handlerMap) {
        this.handlerMap = handlerMap;
    }
}
