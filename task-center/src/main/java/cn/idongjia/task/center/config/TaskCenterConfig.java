package cn.idongjia.task.center.config;

/**
 * Created by renxianlong on 16/4/5.
 */
public class TaskCenterConfig {
    //集群名称,各集群节点需要保证集群名称一致,以保证注册中心将整个集群统一管理
    private String clusterName = "task_cluster";
    //注册中心地址,即ZK地址
    private String registAddr = "127.0.0.1:2181";
    //服务监听端口
    private int listenPort = 35002;
    //设置业务日志记录, 可选值: mongo, mysql,console, 推荐使用 mongo
    private String jobLogger = "mysql";
    //任务队列保存地址
    private String jobQueue = "mysql";
    //MySql相关配置
    private String mysqlHost = "localhost";
    private int mysqlPort = 3306;
    private String mysqlDataBase = "kaipao";
    private String mysqlUserName = "kaipao";
    private String mysqlPasswd = "kaipao";
    //MONGO相关配置
    private String mongoAddress = "127.0.0.1:27017";
    private String mongoDatabase = "task";

    public String getClusterName() {
        return clusterName;
    }

    public void setClusterName(String clusterName) {
        this.clusterName = clusterName;
    }

    public String getRegistAddr() {
        return registAddr;
    }

    public void setRegistAddr(String registAddr) {
        this.registAddr = registAddr;
    }

    public int getListenPort() {
        return listenPort;
    }

    public void setListenPort(int listenPort) {
        this.listenPort = listenPort;
    }

    public String getJobLogger() {
        return jobLogger;
    }

    public void setJobLogger(String jobLogger) {
        this.jobLogger = jobLogger;
    }

    public String getJobQueue() {
        return jobQueue;
    }

    public void setJobQueue(String jobQueue) {
        this.jobQueue = jobQueue;
    }

    public String getMysqlHost() {
        return mysqlHost;
    }

    public void setMysqlHost(String mysqlHost) {
        this.mysqlHost = mysqlHost;
    }

    public int getMysqlPort() {
        return mysqlPort;
    }

    public void setMysqlPort(int mysqlPort) {
        this.mysqlPort = mysqlPort;
    }

    public String getMysqlDataBase() {
        return mysqlDataBase;
    }

    public void setMysqlDataBase(String mysqlDataBase) {
        this.mysqlDataBase = mysqlDataBase;
    }

    public String getMysqlUserName() {
        return mysqlUserName;
    }

    public void setMysqlUserName(String mysqlUserName) {
        this.mysqlUserName = mysqlUserName;
    }

    public String getMysqlPasswd() {
        return mysqlPasswd;
    }

    public void setMysqlPasswd(String mysqlPasswd)
    {
        this.mysqlPasswd = mysqlPasswd;
    }

    public String getMongoAddress() {
        return mongoAddress;
    }

    public void setMongoAddress(String mongoAddress) {
        this.mongoAddress = mongoAddress;
    }

    public String getMongoDatabase() {
        return mongoDatabase;
    }

    public void setMongoDatabase(String mongoDatabase) {
        this.mongoDatabase = mongoDatabase;
    }
}
