package cn.idongjia.task.center;

import cn.idongjia.task.center.config.TaskCenterConfig;
import cn.idongjia.task.common.siginal.Stopable;
import com.lts.jobtracker.JobTracker;

/**
 * Created by renxianlong on 16/4/5.
 */
public class TaskCenter implements Stopable{
    JobTracker delegate;

    public TaskCenter(TaskCenterConfig config) {
        delegate = new JobTracker();
        delegate.setClusterName(config.getClusterName());
        delegate.setListenPort(config.getListenPort());
        delegate.setRegistryAddress(config.getRegistAddr());
        delegate.addConfig("job.logger", config.getJobLogger());
        delegate.addConfig("job.queue", config.getJobQueue());
        delegate.addConfig("jdbc.url", generateJdbcUrl(config)); //
        delegate.addConfig("jdbc.username", config.getMysqlUserName());
        delegate.addConfig("jdbc.password", config.getMysqlPasswd());
        delegate.addConfig("mongo.addresses", config.getMongoAddress());
        delegate.addConfig("mongo.database", config.getMongoDatabase());
    }

    private String generateJdbcUrl(TaskCenterConfig config) {
        //"jdbc:mysql://10.252.84.155:3306/kaipao"
        StringBuilder build = new StringBuilder();
        build.append("jdbc:mysql://");
        build.append(config.getMysqlHost());
        build.append(":");
        build.append(config.getMysqlPort());
        build.append("/");
        build.append(config.getMysqlDataBase());
        return build.toString();
    }

    public void start() {
        delegate.start();
    }

    @Override
    public void stop() {
        delegate.stop();
    }
}
