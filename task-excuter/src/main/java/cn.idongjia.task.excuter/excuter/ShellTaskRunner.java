package cn.idongjia.task.excuter.excuter;

import cn.idongjia.task.common.task.*;
import cn.idongjia.task.common.util.TimeUtil;
import cn.idongjia.task.excuter.Constant;
import cn.idongjia.task.excuter.example.LogTaskExcuter;
import cn.idongjia.task.excuter.shell.ShellUtil;
import com.lts.core.domain.Action;
import com.lts.core.json.JSON;
import com.lts.tasktracker.Result;
import org.apache.log4j.Logger;

import java.util.Map;

/**
 * Created by renxianlong on 16/4/8.
 */
public class ShellTaskRunner implements TaskRunner{

    public static final String LOGTASK = "logTask";

    Logger logger = Logger.getLogger(ShellTaskRunner.class);

    @Override
    public ExcuteResult run(Task task) {
        logger.info(TimeUtil.getCurrentTime() + ":收到任务 " + task + ",开始执行");
        long beginTm = System.currentTimeMillis();
        if(ExcuteType.SHELL.equals(task.getExcuteType())){
            return ShellUtil.getInstance().executeShell(task.getShell());
        }
        else{
            LogTaskExcuter.getInstance().excute(task);
        }
        long endTm = System.currentTimeMillis();
        logger.info(TimeUtil.getCurrentTime() + ":任务执行结束,耗时" + (endTm-beginTm) + "ms");
        return new ExcuteResult();
    }
}
