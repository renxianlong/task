package cn.idongjia.task.excuter.dispacher;

import cn.idongjia.task.common.task.TaskRunner;
import cn.idongjia.task.excuter.Constant;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by renxianlong on 16/4/8.
 */
public class TaskRunnerManager {
    public static final String DEFAULT_TASK_TYPE = "default_task";

    public static TaskRunnerManager instance = new TaskRunnerManager();
    //任务执行者,根据不同的jobtype可以自己注册不同的TaskRunner
    private Map<String,TaskRunner> taskRunnerMap = new ConcurrentHashMap<String, TaskRunner>();

    public static TaskRunnerManager getInstance(){
        return instance;
    }

    private TaskRunnerManager(){
    }

    public void registTaskRuner(String taskType, TaskRunner runner){
        taskRunnerMap.put(taskType,runner);
    }

    public void registTaskRuners(Map<String,TaskRunner> runnerMap){
        taskRunnerMap.putAll(runnerMap);
    }

    public void registDefaultTaskRuner(TaskRunner runner){
        taskRunnerMap.put(DEFAULT_TASK_TYPE,runner);
    }

    public TaskRunner getTaskRunner(String taskType){
        if(null != taskType){
            TaskRunner runner = taskRunnerMap.get(taskType);
            if(null != runner){
               return runner;
            }
        }
        return taskRunnerMap.get(DEFAULT_TASK_TYPE);
    }
}
