package cn.idongjia.task.excuter.init;

import cn.idongjia.task.common.siginal.Stopable;
import cn.idongjia.task.common.task.TaskRunner;
import cn.idongjia.task.excuter.Constant;
import cn.idongjia.task.excuter.config.TaskExcuterConfig;
import cn.idongjia.task.excuter.dispacher.TaskDispacher;
import cn.idongjia.task.excuter.dispacher.TaskRunnerManager;
import com.lts.core.commons.utils.CollectionUtils;
import com.lts.spring.tasktracker.JobDispatcher;
import com.lts.tasktracker.TaskTracker;

/**
 * Created by renxianlong on 16/4/8.
 */
public class TaskExcuter extends TaskExcuterConfig implements Stopable {

    private TaskTracker delegate;

    public TaskExcuter(){
        System.out.println("asdsd");
    }

    public void initExcuter(TaskExcuterConfig config) {
        delegate = new TaskTracker();
        delegate.setClusterName(config.getClusterName());
        delegate.setRegistryAddress(config.getRegistAddr());
        delegate.setNodeGroup(config.getTaskExcuterGroup());
        delegate.setIdentity(config.getTaskExcuterName());
        delegate.setWorkThreads(config.getWorkThreads());
        delegate.setJobRunnerClass(TaskDispacher.class);

        TaskRunnerManager.getInstance().registTaskRuners(config.getTaskRunnerMap());
    }

    //用于spring bean启动
    public void init() {
        System.out.println("fucj");
        initExcuter(this);
        start();
    }

    public void start() {
        delegate.start();
    }

    @Override
    public void stop() {
        delegate.stop();
    }

}
