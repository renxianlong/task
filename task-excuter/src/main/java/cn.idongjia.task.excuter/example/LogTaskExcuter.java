package cn.idongjia.task.excuter.example;

import cn.idongjia.task.common.task.Task;
import org.apache.log4j.Logger;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by renxianlong on 16/4/15.
 */
public class LogTaskExcuter {

    public static SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss:SSS");

    private static LogTaskExcuter instance = new LogTaskExcuter();

    Logger logger = Logger.getLogger(LogTaskExcuter.class);

    Logger task1Log = Logger.getLogger("task1");

    Logger task2Log = Logger.getLogger("task2");

    Logger task3Log = Logger.getLogger("task3");

    Logger task4Log = Logger.getLogger("task4");

    Logger task5Log = Logger.getLogger("task5");

    Logger task6Log = Logger.getLogger("task6");

    public void excute(Task task){
         String logFile = task.getExtParam("logfile");
         Logger loggerTemp = null;
         if(null == logFile){
             loggerTemp = this.logger;
         }else if("task1".equals(logFile)){
             loggerTemp = task1Log;
         }else if("task2".equals(logFile)){
             loggerTemp = task2Log;
         }else if("task3".equals(logFile)){
             loggerTemp = task3Log;
         }else if("task4".equals(logFile)){
             loggerTemp = task4Log;
         }else if("task5".equals(logFile)){
             loggerTemp = task5Log;
         }else if("task6".equals(logFile)){
             loggerTemp = task6Log;
         }

         loggerTemp.debug(format.format(new Date()) + ":excuter task " + task + "at " + format.format(new Date()) + " triggerTm : " + format.format(task.getTriggerTime()));
     }

    private LogTaskExcuter(){

    }

    public static LogTaskExcuter getInstance(){
        return instance;
    }
}
