package cn.idongjia.task.excuter.config;

import cn.idongjia.task.common.task.TaskRunner;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by renxianlong on 16/4/5.
 */
public class TaskExcuterConfig {
    //集群名称,各集群节点需要保证集群名称一致,以保证注册中心将整个集群统一管理
    private String clusterName = "task_cluster";
    //注册中心地址,即ZK地址
    private String registAddr = "zookeeper://127.0.0.1:2181";
    //任务执行者归属组
    private String taskExcuterGroup = "DefaultTaskExcuterGroup";
    //任务执行者名称
    private String taskExcuterName = "DefaultTaskExcuter";
    //工作线程, 目前只对 TaskExcuter 有效
    private int workThreads = Runtime.getRuntime().availableProcessors();

    private Map<String,TaskRunner> taskRunnerMap = new HashMap<String, TaskRunner>();

    public String getClusterName() {
        return clusterName;
    }

    public void setClusterName(String clusterName) {
        this.clusterName = clusterName;
    }

    public String getRegistAddr() {
        return registAddr;
    }

    public void setRegistAddr(String registAddr) {
        this.registAddr = registAddr;
    }

    public String getTaskExcuterGroup() {
        return taskExcuterGroup;
    }

    public void setTaskExcuterGroup(String taskExcuterGroup) {
        this.taskExcuterGroup = taskExcuterGroup;
    }

    public String getTaskExcuterName() {
        return taskExcuterName;
    }

    public void setTaskExcuterName(String taskExcuterName) {
        this.taskExcuterName = taskExcuterName;
    }

    public int getWorkThreads() {
        return workThreads;
    }

    public void setWorkThreads(int workThreads)
    {
        this.workThreads = workThreads;
    }

    public Map<String, TaskRunner> getTaskRunnerMap() {
        return taskRunnerMap;
    }

    public void setTaskRunnerMap(Map<String, TaskRunner> taskRunnerMap) {
        this.taskRunnerMap = taskRunnerMap;
    }
}
