#!/bin/sh

# OS specific support.  $var _must_ be set to either true or false.
cygwin=false
darwin=false
os400=false
case "`uname`" in
CYGWIN*) cygwin=true;;
Darwin*) darwin=true;;
OS400*) os400=true;;
esac


# resolve links - $0 may be a softlink
PRG="$0"

while [ -h "$PRG" ]; do
  ls=`ls -ld "$PRG"`
  link=`expr "$ls" : '.*-> \(.*\)$'`
  if expr "$link" : '/.*' > /dev/null; then
    PRG="$link"
  else
    PRG=`dirname "$PRG"`/"$link"
  fi
done

# Get standard environment variables
PRGDIR=`dirname "$PRG"`

# Only set TASK_HOME if not already set
[ -z "$TASK_HOME" ] && TASK_HOME=`cd "$PRGDIR/.." >/dev/null; pwd`

# Only set TASK_LIB if not already set
[ -z "$TASK_LIB" ] && TASK_LIB=`cd "$PRGDIR/../lib" >/dev/null; pwd`

# Only set TASK_TASKEXCUTERJAR if not already set
[ -z "$TASK_TASKEXCUTERJAR" ] && TASK_TASKEXCUTERJAR=$TASK_LIB/task-djexcuter-1.0.jar

# Only set $TASK_OUT if not already set
if [ -z "$TASK_OUT" ] ; then
  TASK_OUT="$TASK_HOME"/log/taskexcuter.log
fi

# Make sure prerequisite environment variables are set
if [ -z "$JAVA_HOME" -a -z "$JRE_HOME" ]; then
  if $darwin; then
    # Bugzilla 54390
    if [ -x '/usr/libexec/java_home' ] ; then
      export JAVA_HOME=`/usr/libexec/java_home`
    # Bugzilla 37284 (reviewed).
    elif [ -d "/System/Library/Frameworks/JavaVM.framework/Versions/CurrentJDK/Home" ]; then
      export JAVA_HOME="/System/Library/Frameworks/JavaVM.framework/Versions/CurrentJDK/Home"
    fi
  else
    JAVA_PATH=`which java 2>/dev/null`
    if [ "x$JAVA_PATH" != "x" ]; then
      JAVA_PATH=`dirname $JAVA_PATH 2>/dev/null`
      JRE_HOME=`dirname $JAVA_PATH 2>/dev/null`
    fi
    if [ "x$JRE_HOME" = "x" ]; then
      # XXX: Should we try other locations?
      if [ -x /usr/bin/java ]; then
        JRE_HOME=/usr
      fi
    fi
  fi
  if [ -z "$JAVA_HOME" -a -z "$JRE_HOME" ]; then
    echo "Neither the JAVA_HOME nor the JRE_HOME environment variable is defined"
    echo "At least one of these environment variable is needed to run this program"
    exit 1
  fi
fi
if [ -z "$JAVA_HOME" -a "$1" = "debug" ]; then
  echo "JAVA_HOME should point to a JDK in order to run in debug mode."
  exit 1
fi
if [ -z "$JRE_HOME" ]; then
  JRE_HOME="$JAVA_HOME"
fi

# Set standard commands for invoking Java, if not already set.
if [ -z "$_RUNJAVA" ]; then
  _RUNJAVA="$JRE_HOME"/bin/java
fi



if [ "$1" = "start" ] ; then
 touch "$TASK_OUT"
 eval "\"$_RUNJAVA\"" \
      -Dtask.home="\"$TASK_HOME\"" \
      -jar "\"$TASK_TASKEXCUTERJAR\"" \
       >> "$TASK_OUT" 2>&1 "&"

elif [ "$1" = "stop" ] ; then
  pid=`eval ps ax | grep -i $TASK_TASKEXCUTERJAR |grep java | grep -v grep | awk '{print $1}'`
  if [ -z "$pid" ] ; then
            echo "No taskexcuter running."
            exit -1;
  fi
  echo "The taskexcuter(${pid}) is running..."

  kill -15 ${pid}

  echo "Send shutdown request to taskexcuter(${pid}) OK"
fi