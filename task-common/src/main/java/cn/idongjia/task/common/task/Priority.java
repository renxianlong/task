package cn.idongjia.task.common.task;

/**
 * Created by renxianlong on 16/4/7.
 */
public enum Priority {
    HIGH(10),
    MIDDLE(100),
    LOW(1000);

    private int value;

    private Priority(int value){
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public static Priority fromValue(int value){
       if(HIGH.getValue() == value){
          return HIGH;
       }else if(LOW.getValue() == value){
          return LOW;
       }else {
           return MIDDLE;
       }
    }
}
