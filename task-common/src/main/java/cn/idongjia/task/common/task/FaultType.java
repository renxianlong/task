package cn.idongjia.task.common.task;

/**
 * Created by renxianlong on 16/4/7.
 */
public enum FaultType {
    LOST,
    RETRY
}
