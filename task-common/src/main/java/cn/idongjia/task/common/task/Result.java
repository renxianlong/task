package cn.idongjia.task.common.task;

/**
 * Created by renxianlong on 16/4/8.
 */
public enum Result {
    EXECUTE_SUCCESS,    // 执行成功,这种情况 直接反馈客户端
    EXECUTE_FAILED,     // 执行失败,这种情况,直接反馈给客户端,不重新执行
}
