package cn.idongjia.task.common.task;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.lts.core.commons.utils.CollectionUtils;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by renxianlong on 16/4/7.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Task implements Serializable{
    //任务执行类型,默认java
    private ExcuteType excuteType = ExcuteType.JAVA;
    //任务类型,默认实时任务
    private Type type = Type.REAL;
    //任务优先级,默认普通优先级
    private Priority priority = Priority.MIDDLE;
    //任务执行者,JAVA任务必填,其他任务不用填
    private String exuterName;
    //任务的具体id,每条任务的唯一标示
    private String taskId;
    //任务类型,执行端根据任务类型选择执行方法
    private String taskType;
    //任务完成是否需要处理反馈结果,默认不需要反馈
    private boolean needFeedBack = false;
    //任务失败处理方式,默认丢弃
    private FaultType faultType = FaultType.LOST;
    //任务失败重试次数,默认只重试一次
    private int retryTm = 1;
    //cron表达式,cron任务必填
    private String cronExpression;
    //任务创建时间
    private Long createTm = System.currentTimeMillis();
    //Shell脚本,在执行类型为shell的时候需要在这里填写具体的shell脚本
    private String shell;
    //任务的触发时间
    private Long triggerTime = System.currentTimeMillis();
    //具体任务参数,应用业务侧自定义
    private Map<String, String> extParams;

    public ExcuteType getExcuteType() {
        return excuteType;
    }

    public void setExcuteType(ExcuteType excuteType) {
        this.excuteType = excuteType;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Priority getPriority() {
        return priority;
    }

    public void setPriority(Priority priority) {
        this.priority = priority;
    }

    public String getExuterName() {
        return exuterName;
    }

    public void setExuterName(String exuterName) {
        this.exuterName = exuterName;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getTaskType() {
        return taskType;
    }

    public void setTaskType(String taskType) {
        this.taskType = taskType;
    }

    public boolean isNeedFeedBack() {
        return needFeedBack;
    }

    public void setNeedFeedBack(boolean needFeedBack) {
        this.needFeedBack = needFeedBack;
    }

    public FaultType getFaultType() {
        return faultType;
    }

    public void setFaultType(FaultType faultType) {
        this.faultType = faultType;
    }

    public int getRetryTm() {
        return retryTm;
    }

    public void setRetryTm(int retryTm) {
        this.retryTm = retryTm;
    }

    public String getCronExpression() {
        return cronExpression;
    }

    public void setCronExpression(String cronExpression) {
        this.cronExpression = cronExpression;
    }

    public long getCreateTm() {
        return createTm;
    }

    public Long getTriggerTime() {
        return triggerTime;
    }

    public void setTriggerTime(Long triggerTime)
    {
        this.triggerTime = triggerTime;
    }

    public void setCreateTm(Long createTm) {
        this.createTm = createTm;
    }

    public Map<String, String> getExtParams() {
        return extParams;
    }

    public String getExtParam(String paramName){
        if (CollectionUtils.isNotEmpty(extParams)) {
            return extParams.get(paramName);
        }
        return null;
    }

    public void setExtParam(String paramName,String paramValue){
        if (null == extParams) {
            extParams = new HashMap<>();
        }
        extParams.put(paramName,paramValue);
    }

    public void setExtParams(Map<String, String> extParams) {
        this.extParams = extParams;
    }

    public String getShell() {
        return shell;
    }

    public void setShell(String shell) {
        this.shell = shell;
    }

    @Override
    public String toString() {
        return "Task{" +
                "excuteType=" + excuteType +
                ", type=" + type +
                ", priority=" + priority +
                ", exuterName='" + exuterName + '\'' +
                ", taskId='" + taskId + '\'' +
                ", taskType='" + taskType + '\'' +
                ", needFeedBack=" + needFeedBack +
                ", faultType=" + faultType +
                ", retryTm=" + retryTm +
                ", cronExpression='" + cronExpression + '\'' +
                ", createTm=" + createTm +
                ", shell='" + shell + '\'' +
                ", extParams=" + extParams +
                ", triggerTime=" + triggerTime +
                '}';
    }
}
