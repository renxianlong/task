package cn.idongjia.task.common.util;

/**
 * Created by renxianlong on 16/4/22.
 */

import java.io.*;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class PropertiesUtil {

    /**
     * 根据key值读取配置的值
     * Jun 26, 2010 9:15:43 PM
     * @param key key值
     * @return key 键对应的值
     * @throws IOException
     */
    public static String readValue(String configPath , String key){
        BufferedInputStream in = null;
        try {
            in = new BufferedInputStream(new FileInputStream(configPath));
            Properties properties = new Properties();
            properties.load(in);
            return  properties.getProperty(key);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e){
            e.printStackTrace();
        }
        return null;
    }


    /**
     * 设置某个key的值,并保存至文件。
     * Jun 26, 2010 9:15:43 PM
     * @author 朱志杰
     * @param key key值
     * @return key 键对应的值
     * @throws IOException
     */
    public static void setValue(String configPath, String key,String value) throws IOException {
        Properties prop = new Properties();
        InputStream fis = new FileInputStream(configPath);
        // 从输入流中读取属性列表（键和元素对）
        prop.load(fis);
        // 调用 Hashtable 的方法 put。使用 getProperty 方法提供并行性。
        // 强制要求为属性的键和值使用字符串。返回值是 Hashtable 调用 put 的结果。
        OutputStream fos = new FileOutputStream(configPath);
        prop.setProperty(key, value);
        // 以适合使用 load 方法加载到 Properties 表中的格式，
        // 将此 Properties 表中的属性列表（键和元素对）写入输出流
        prop.store(fos,"last update");
        //关闭文件
        fis.close();
        fos.close();
    }
}
