package cn.idongjia.task.common.exception;

/**
 * Created by renxianlong on 16/4/22.
 */
public class TaskException extends RuntimeException{
    private String msg;

    public TaskException(String msg){
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
