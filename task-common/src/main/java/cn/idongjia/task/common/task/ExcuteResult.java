package cn.idongjia.task.common.task;

/**
 * Created by renxianlong on 16/4/8.
 */
public class ExcuteResult {
    public String msg;
    public Result result = Result.EXECUTE_SUCCESS;

    public ExcuteResult() {
    }

    public ExcuteResult(String msg) {
        this.msg = msg;
    }

    public ExcuteResult(Result result, String msg){
        this.msg = msg;
        this.result = result;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }
}
