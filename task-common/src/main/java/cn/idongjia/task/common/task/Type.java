package cn.idongjia.task.common.task;

/**
 * Created by renxianlong on 16/4/7.
 */
public enum  Type {
    //实时任务
    REAL,
    //定时任务
    TIMING,
    //CRON任务
    CRON
}
