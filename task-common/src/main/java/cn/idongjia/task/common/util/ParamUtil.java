package cn.idongjia.task.common.util;

import cn.idongjia.task.common.exception.TaskException;
import cn.idongjia.task.common.task.Task;
import com.lts.core.commons.utils.StringUtils;

/**
 * Created by renxianlong on 16/4/22.
 */
public class ParamUtil {

    public static void checkMandatory(String value, String name){
        if(StringUtils.isEmpty(value)){
            throw new TaskException("missing mandatory param " + name);
        }
    }

    public static void checkMandatory(Object value, String name){
        if(null == value){

        }
    }

    public static void checkMandatory(Task task,String... names){
        for (String name : names){
            if(StringUtils.isEmpty(task.getExtParam(name))){
                throw new TaskException("missing mandatory param " + name);
            }
        }
    }
}
