package cn.idongjia.task.common.util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by renxianlong on 16/4/8.
 */
public class TimeUtil {

    public static SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh24:mm:ss:SSS");

    public static String getCurrentTime(){
        return format.format(new Date());
    }
}
