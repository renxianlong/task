package cn.idongjia.task.common.services;
//
//import cn.idongjia.task.common.task.Task;

import cn.idongjia.task.common.task.Task;

/**
 * Created by renxianlong on 16/4/20.
 */
public interface TaskService {
    /*
        提交任务
     */
    void submitTask(Task task);

    /*
        取消任务
     */
    void cancelTask(String taskId);

    /*
        更新任务
     */
    void updateTask(Task task);
}
