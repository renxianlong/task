package cn.idongjia.task.common.task;

/**
 * Created by renxianlong on 16/4/7.
 */
public class TaskResult {
    //具体任务
    private Task task;
    // 执行成功还是失败
    private boolean success;
    // 详细信息
    private String msg;
    // 任务完成时间
    private Long time;

    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time)
    {
        this.time = time;
    }

    @Override
    public String toString() {
        return "TaskResult{" +
                "task=" + task +
                ", success=" + success +
                ", msg='" + msg + '\'' +
                ", time=" + time +
                '}';
    }
}
