package cn.idongjia.task.common.constant;

/**
 * Created by renxianlong on 16/4/8.
 */
public class ModulePath {
    private static final String ROOT_PATH = System.getProperty("user.dir");
    public static final String TASK_CENTER = ROOT_PATH + "/task-center";
    public static final String TASK_ADMIN = ROOT_PATH + "/task-admin";
    public static final String TASK_EXCUTER = ROOT_PATH + "/task-excuter";
    public static final String TASK_PROVIDER = ROOT_PATH + "/task-provider";
}
