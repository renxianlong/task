package cn.idongjia.task.common.constant;

/**
 * Created by renxianlong on 16/4/22.
 */
public class RedisDb {
    public static final int DB0 = 0;
    public static final int DB1 = 1;
    public static final int DB2 = 2;
    public static final int DB3 = 3;
    public static final int DB4 = 4;
    public static final int DB5 = 5;
}
