package cn.idongjia.task.common.siginal;

/**
 * Created by renxianlong on 16/4/8.
 */
public interface Stopable {
    void stop();
}
