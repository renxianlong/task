package cn.idongjia.task.common.siginal;

import org.apache.log4j.Logger;
import sun.misc.Signal;
import sun.misc.SignalHandler;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by renxianlong on 16/4/6.
 */
public class ShutDownHandler implements SignalHandler {

    private Stopable stoper;

    public static SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh24:mm:ss:SSS");

    private Logger logger = Logger.getLogger(ShutDownHandler.class);

    public ShutDownHandler(Stopable stoper) {
        this.stoper = stoper;
    }

    @Override
    public void handle(Signal signal) {
        long beginTm = new Date().getTime();
        logger.info(format.format(beginTm) + ":收到" + signal.getName() + "信号,系统开始停机");
        stoper.stop();
        long endTm = new Date().getTime();
        logger.info(format.format(endTm) + ":系统停机结束,耗时" + (endTm - beginTm) + "ms");
    }
}
