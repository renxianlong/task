package cn.idongjia.task.common.task;

import com.lts.core.domain.Job;

/**
 * Created by renxianlong on 16/4/8.
 */
public interface TaskRunner {
    /**
     * 执行任务
     */
    public ExcuteResult run(Task task);
}
