package cn.idongjia.task.djexcuter.mapper;


import cn.idongjia.task.djexcuter.pojo.Post;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;


import java.util.List;

@Component("postMapper")
public interface PostMapper {

    @Insert("INSERT INTO kp_post (uid,createtm,pictures,content,type,title,status,recommend) VALUES" +
            " (#{uid}, #{createtm}, #{pictures}, #{content}, #{type}, #{title}, #{status}, #{recommend})")
    @Options(useGeneratedKeys = true, keyProperty = "pid")
    int insert(Post post);

    @Select("SELECT * FROM kp_post WHERE pid=#{id}")
    Post getByCode(Long pid);

    @Update("<script>" +
            " UPDATE `kp_post` SET `pid` = #{pid}" +
            " <if test=\"pictures != null\"> , `pictures` = #{pictures}</if>" +
            " <if test=\"content != null\"> , `content` = #{content}</if>" +
            " <if test=\"type != null\"> , `type` = #{type}</if>" +
            " <if test=\"title != null\"> , `title` = #{title}</if>" +
            " <if test=\"recommend != null\"> , `recommend` = #{recommend}</if>" +
            " <if test=\"recommendtm != null\"> , `recommendtm` = #{recommendtm}</if>" +
            " <if test=\"status != null\"> , `status` = #{status}</if>" +
            " <if test=\"replycnt != null\"> , `replycnt` = #{replycnt}</if>" +
            " <if test=\"collectcnt != null\"> , `collectcnt` = #{collectcnt}</if>" +
            " <if test=\"weight != null\"> , `weight` = #{weight}</if>" +
            " WHERE `pid` = #{pid}" +
            "</script>")
    int update(Post post);

    @Select("<script>" +
            "SELECT COUNT(*) total FROM kp_post " +
            " <where>" +
            " 1=1" +
            " </where>" +
            "</script>")
    Integer count();

    @Update("UPDATE kp_post set status=-1 WHERE pid=#{id}")
    int delete(Long id);

}
