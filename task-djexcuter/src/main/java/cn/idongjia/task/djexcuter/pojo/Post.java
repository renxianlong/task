package cn.idongjia.task.djexcuter.pojo;

public class Post {
    private Long pid;
    private Long uid;
    private Long createtm;
    private String pictures = "";
    private String content = "";
    private Integer type = Type.ITEM.code;
    private Long replycnt = 0L;
    private Long hatecnt = 0L;
    private Integer weight = 0;
    private Integer status = 0;
    private Integer recommend = 0;
    private Long recommendtm;
    private Integer collectcnt = 0;
    private String title = "";

    public enum Type {
        POST(1), NOTIFY(2), ITEM(3);

        private Integer code;
        Type(int code) {
            this.code = code;
        }

        public Integer get() {
            return code;
        }
    }

    public Long getPid() {
        return pid;
    }

    public void setPid(Long pid) {
        this.pid = pid;
    }

    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }

    public Long getCreatetm() {
        return createtm;
    }

    public void setCreatetm(Long createtm) {
        this.createtm = createtm;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Long getReplycnt() {
        return replycnt;
    }

    public void setReplycnt(Long replycnt) {
        this.replycnt = replycnt;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Long getHatecnt() {
        return hatecnt;
    }

    public void setHatecnt(Long hatecnt) {
        this.hatecnt = hatecnt;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getRecommend() {
        return recommend;
    }

    public void setRecommend(Integer recommend) {
        this.recommend = recommend;
    }

    public Integer getCollectcnt() {
        return collectcnt;
    }

    public void setCollectcnt(Integer collectcnt) {
        this.collectcnt = collectcnt;
    }

    public String getPictures() {
        return pictures;
    }

    public void setPictures(String pictures) {
        this.pictures = pictures;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public Long getRecommendtm() {
        return recommendtm;
    }

    public void setRecommendtm(Long recommendtm) {
        this.recommendtm = recommendtm;
    }
}
