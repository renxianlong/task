package cn.idongjia.task.djexcuter.handler;

import cn.idongjia.task.common.task.ExcuteResult;
import cn.idongjia.task.common.task.Result;
import cn.idongjia.task.common.task.Task;
import cn.idongjia.task.common.task.TaskRunner;
import cn.idongjia.task.djexcuter.mapper.PostMapper;
import cn.idongjia.task.djexcuter.pojo.Post;
import javafx.geometry.Pos;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by renxianlong on 16/4/18.
 */

public class TimerRecommendHandler extends AbstractExcuterHandler{

    @Resource
    private PostMapper postMapper;

    private Logger logger = Logger.getLogger(TimerRecommendHandler.class);

    public static SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss:SSS");

    @Override
    @Transactional
    public ExcuteResult excute(Task task) {
        ExcuteResult result = new ExcuteResult();
        logger.debug(format.format(new Date()) + ":收到定时加精任务 " + task);
        String pid = task.getExtParam("pid");
        if(StringUtils.isEmpty(pid)){
            logger.debug("加精失败,pid为空");
            result.setResult(Result.EXECUTE_FAILED);
            return result;
        }
        long lpid = Long.parseLong(pid);
        Post post = postMapper.getByCode(lpid);
        if(null == post){
            logger.debug("加精失败,根据pid" + pid + "找不到对应的post数据");
            result.setResult(Result.EXECUTE_FAILED);
            return result;
        }
        post.setRecommend(1);
        post.setRecommendtm(System.currentTimeMillis()/1000);
        postMapper.update(post);
        logger.debug(format.format(new Date()) + ":加精成功,任务执行结束");
        return result;
    }
}
