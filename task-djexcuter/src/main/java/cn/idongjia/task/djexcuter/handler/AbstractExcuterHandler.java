package cn.idongjia.task.djexcuter.handler;

import cn.idongjia.task.common.task.ExcuteResult;
import cn.idongjia.task.common.task.Task;
import cn.idongjia.task.common.task.TaskRunner;
import org.apache.log4j.Logger;

/**
 * Created by renxianlong on 16/4/22.
 */
public abstract class AbstractExcuterHandler implements TaskRunner{
    private Logger logger = Logger.getLogger(AbstractExcuterHandler.class);

    @Override
    public ExcuteResult run(Task task) {
        logger.info("begin to run task " + task);
        ExcuteResult result = excute(task);
        logger.info("end to run task " + task.getTaskId());
        return result;
    }

    public abstract ExcuteResult excute(Task task);
}
