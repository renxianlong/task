package cn.idongjia.task.djexcuter.handler;

import cn.idongjia.article.pojo.TabRecommend;
import cn.idongjia.article.services.ArticleManageService;
import cn.idongjia.article.services.ArticleService;
import cn.idongjia.task.common.task.ExcuteResult;
import cn.idongjia.task.common.task.Result;
import cn.idongjia.task.common.task.Task;
import cn.idongjia.task.djexcuter.mapper.PostMapper;
import cn.idongjia.task.djexcuter.pojo.Post;
import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TimerArticleHandler extends AbstractExcuterHandler{
    private static final Logger LOGGER = Logger.getLogger(TimerArticleHandler.class);

    @Resource
    private ArticleManageService service;

    @Override
    @Transactional
    public ExcuteResult excute(Task task) {
        ExcuteResult result = new ExcuteResult();

        TabRecommend recommend = new TabRecommend();
        recommend.setTid(Long.valueOf(task.getExtParam("tid")));
        recommend.setType(Integer.valueOf(task.getExtParam("type")));
        recommend.setId(Long.valueOf(task.getExtParam("id")));
        recommend.setWeight(Integer.valueOf(task.getExtParam("weight")));
        recommend.setCreatetm(Long.valueOf(task.getExtParam("recommendtm")));

        service.addRecommends(recommend);

        return result;
    }
}
