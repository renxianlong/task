package cn.idongjia.task.djexcuter;

import cn.idongjia.task.common.command.CommandUtil;
import cn.idongjia.task.common.util.PropertiesUtil;
import cn.idongjia.task.excuter.init.TaskExcuter;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

/**
 * Created by renxianlong on 16/4/18.
 */
public class DongjiaTaskExcuter {
    public static SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss:SSS");

    public static Logger logger = Logger.getLogger(DongjiaTaskExcuter.class);

    public static String ROOT_PATH = System.getProperty("task.home");

    public static String CONF_FILE = ROOT_PATH + "/conf/task-djexcuter.properties";

    public static ClassPathXmlApplicationContext context;

    public static void main(String[] args) {

        long beginTm = new Date().getTime();
        logger.info(format.format(beginTm) + ":系统开始启动");

//        初始化disconf配置
//        intDisconf();

        //启动spring
        context = new ClassPathXmlApplicationContext("djexcuter.xml");//读取bean.xml中的内容
        context.start();

        long endTm = new Date().getTime();
        long elapse = endTm - beginTm;
        logger.info(format.format(endTm) + ":系统启动成功,耗时:" + elapse);
    }

    private static void intDisconf() {
        if(new File(CONF_FILE).exists()){
            BufferedInputStream in = null;
            try {
                in = new BufferedInputStream(new FileInputStream(CONF_FILE));
                Properties properties = new Properties();
                properties.load(in);
                System.setProperty("disconf.env",properties.getProperty("disconf.env"));
                System.setProperty("disconf.conf_server_host",properties.getProperty("disconf.conf_server_host"));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
