package cn.idongjia.task.djexcuter.mapper;

import cn.idongjia.task.djexcuter.pojo.Craftsman;
import cn.idongjia.tianji.pojos.User;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component("userMapper")
public interface UserMapper {

    @Select("select * from kp_user where uid=#{uid}")
    Map<String, String> getUserInfo(long uid);

    @Select("select * from kp_user where uid=#{uid}")
    User getUserInfoByPojo(long uid);

    @Update("update kp_user set status=#{status} where uid=#{uid}")
    void updateStatus(@Param("status") Integer status, @Param("uid") Long uid);

    @Update("update kp_user set adminflag=#{adminflag} where uid=#{uid}")
    void updateAdminFlag(@Param("adminflag") Integer adminflag, @Param("uid") Long uid);

    @Update("update kp_user set conf=#{conf} where uid=#{uid}")
    void updateConf(@Param("conf") String conf, @Param("uid") Long uid);

    @Update("update kp_user set mobile=#{mobile} where uid=#{uid}")
    void updateMobile(@Param("mobile") String mobile, @Param("uid") Long uid);

    @Update("update kp_user set password=#{password} where uid=#{uid}")
    void updatePasswd(@Param("password") String password, @Param("uid") Long uid);

    @Update("update kp_user set salt=#{salt} where uid=#{uid}")
    void updateSalt(@Param("salt") String salt, @Param("uid") Long uid);

    @Update("update kp_user set deleted=1 where uid=#{uid}")
    void delete(@Param("uid") Long uid);

    @Update("update kp_item_order set uid=#{newUid} where uid=#{oldUid}")
    void mergeUserOrder(@Param("newUid") long newUid, @Param("oldUid") long oldUid);

    @Update("update kp_item_order_evaluate set uid=#{newUid} where uid=#{oldUid}")
    void mergeUserOrderEvaluate(@Param("newUid") long newUid, @Param("oldUid") long oldUid);

    @Update("update kp_item_order_log set uid=#{newUid} where uid=#{oldUid}")
    void mergeUserOrderLog(@Param("newUid") long newUid, @Param("oldUid") long oldUid);

    @Update("update kp_item_order set suid=#{newUid} where suid=#{oldUid}")
    void mergeCraftsmanOrder(@Param("newUid") long newUid, @Param("oldUid") long oldUid);

    @Update("update kp_refund set uid=#{newUid} where uid=#{oldUid}")
    void mergeUserRefund(@Param("newUid") long newUid, @Param("oldUid") long oldUid);

    @Update("update kp_refund_log set uid=#{newUid} where uid=#{oldUid}")
    void mergeUserRefundLog(@Param("newUid") long newUid, @Param("oldUid") long oldUid);

    @Update("update kp_refund set suid=#{newUid} where suid=#{oldUid}")
    void mergeCraftsmanRefund(@Param("newUid") long newUid, @Param("oldUid") long oldUid);

    @Select("select * from kp_craftsman where uid=#{uid}")
    Craftsman getCraftsmanByUid(long uid);

    @Delete("delete from kp_craftsman where uid=#{uid}")
    void deleteCraftsman(long uid);

    @Update("update kp_craftsman set uid=#{newUid} where uid=#{oldUid}")
    void mergeCraftsman(@Param("newUid") long newUid, @Param("oldUid") long oldUid);

    @Update("update kp_item set uid=#{newUid} where uid=#{oldUid}")
    void mergeItem(@Param("newUid") long newUid, @Param("oldUid") long oldUid);

    @Update("update kp_item_log set uid=#{newUid} where uid=#{oldUid}")
    void mergeItemLog(@Param("newUid") long newUid, @Param("oldUid") long oldUid);

    @Delete("delete from kp_craftsman_items" +
            " where uid=#{oldUid} and iid in (" +
            "   select iid from (" +
            "       select b.iid from kp_craftsman_items a" +
            "       left join kp_craftsman_items b on b.iid=a.iid " +
            "       where a.uid=#{newUid} and b.uid=#{old}" +
            "   ) c" +
            ")")
    void deleteDistinctCraftsmanItems(@Param("newUid") long newUid, @Param("oldUid") long oldUid);

    @Update("update kp_craftsman_items set uid=#{newUid} where uid=#{oldUid}")
    int mergeCraftsmanItem(@Param("newUid") long newUid, @Param("oldUid") long oldUid);

    @Select(" select u.uid as uid,u.mobile as rmobile,o.authname as authname,a.mobile as amobile" +
            " from kp_user u " +
            " left join kp_oauth o on u.uid=o.uid" +
            " left join kp_user_address a on a.uid=u.uid" +
            " where u.uid=#{uid} and a.status =2")
    Map<String, String> getSMSDate(@Param("uid") long uid);
}
