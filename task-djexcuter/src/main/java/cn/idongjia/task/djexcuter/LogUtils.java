package cn.idongjia.task.djexcuter;

import com.alibaba.druid.pool.DruidDataSource;

/**
 * Created by renxianlong on 16/4/27.
 */
public class LogUtils {

    private DruidDataSource dataSource;

    public LogUtils(){
    }

    public DruidDataSource getDataSource() {
        return dataSource;
    }

    public void setDataSource(DruidDataSource dataSource) {
        this.dataSource = dataSource;
    }

    public void init(){
        System.out.println("databaselog:" + dataSource.getUrl());
        System.out.println("databaselog:" + dataSource.getUsername());
        System.out.println("databaselog:" + dataSource.getPassword());
    }
}
